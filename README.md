<img src="./src/assets/logo-voiceroleplay-light.svg" height="100" />

# Voice Role Pay
**🖥 Front End Project**

[![pipeline status](https://gitlab.com/voiceroleplay/vrp_front/badges/develop/pipeline.svg)](https://gitlab.com/voiceroleplay/vrp_front/-/commits/develop)
[![coverage report](https://gitlab.com/voiceroleplay/vrp_front/badges/develop/coverage.svg)](https://gitlab.com/voiceroleplay/vrp_front/-/commits/develop)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=bugs)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=code_smells)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=coverage)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=ncloc)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=alert_status)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=security_rating)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=sqale_index)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_front&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_front)

Voice Role Play is the App Management for all your roleplay games.<br />
Add your own roleplay or directly join an existant on **Voice Role Play App**.

- Create your own roleplay
- Join an existant roleplay
- Customize your roleplay adventure
- Customize your roleplay characters
- Play your roleplay with **our voice assistant** 🎙

> The project is bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Front End app use:

- [x] React ⚛️
- [x] Bootstrap 🅱
- [x] Redux 🔗
- [x] React Router 🚘
- [x] Firebase Auth 🔐
- [x] Sentry 💬
- [x] SonarCloud 🕵
- [x] Gitlab CI 🚀

## Install

To install all the necessary please run:

### `npm i`

And now run the app:

### `npm run`

In development mode the app will run on 3000 port and open automatically your default browser to [http://localhost:3000](http://localhost:3000).

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
