import React from 'react'
import { render } from '@testing-library/react'
import { Provider } from 'react-redux'

import store from '../redux/store'
import App from '../App'
import { act } from 'react-dom/test-utils'

let container;

beforeEach(() => {
    container = render(
        <Provider store={store}>
            <App />
        </Provider>
    )
});

afterEach(() => {
    container = null;
});

describe('Vérification des redirections des routes', () => {
    test('Doit rediriger automatiquement ver /login', () => {
        const { getByText } = container
        const redirectUrl = '/login'
        // Vérification dans l'URL
        expect(window.location.pathname).toBe(redirectUrl)
        const loginButton = getByText(/Se connecter/i)
        expect(loginButton).toBeInTheDocument()
    })

    test('Doit rediriger vers /register au click du lien "S\'inscrire" et revenir', () => {
        const { getByText, getByTestId } = container
        const baseUrl = '/login'
        const redirectUrl = '/register'
        // Vérification dans l'URL
        const registerButton = getByText(/S'inscrire/i)
        expect(registerButton).toBeInTheDocument()

        // Interact with page
        act(() => {
            // Find the link (perhaps using the text content)
            const goRegisterLink = getByTestId('register-link')
            // Click it
            goRegisterLink.dispatchEvent(new MouseEvent("click", { bubbles: true }))
        });
        // Vérification dans l'URL
        expect(window.location.pathname).toBe(redirectUrl)

        // Interact with page
        act(() => {
            // Find the link (perhaps using the text content)
            const goLoginLink = getByTestId('login-link')
            // Click it
            goLoginLink.dispatchEvent(new MouseEvent("click", { bubbles: true }))
        });
        // Vérification dans l'URL
        expect(window.location.pathname).toBe(baseUrl)
    })
})
