import React from 'react'
import { render } from '@testing-library/react'
import { Provider } from 'react-redux'

import store from '../redux/store'
import App from '../App'

let container;

beforeEach(() => {
    container = render(
        <Provider store={store}>
            <App />
        </Provider>
    )
});

afterEach(() => {
    container = null;
});

// Test de retour page de connexion
describe('Affiche la page de connexion', () => {
  test('Vérification de l\'existance du bouton "Se connecter"', () => {
    const { getByText } = container
    const connectButton = getByText(/Se connecter/i)
    expect(connectButton).toBeInTheDocument()
  })

  test('Vérification de l\'existance du bouton "S\'inscrire"', () => {
    const { getByText } = container
    const goLink = getByText(/S'inscrire/i)
    expect(goLink).toBeInTheDocument()
  })
})
