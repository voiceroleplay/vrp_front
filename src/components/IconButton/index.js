import React from "react"
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default ({
  variant,
  onClick,
  icon,
  iconSize,
  style
}) => (
  <Button
    variant={variant}
    onClick={onClick}
    style={style}
    className="shadow"
  >
    <FontAwesomeIcon icon={icon} size={iconSize}/>
  </Button>
);
