import React, { useState } from "react";
import { Card, Form, CardColumns, Button, Row, Col } from "react-bootstrap";

import { Accordion } from "react-bootstrap";
import ClasseItem from "../ClasseItem";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExchangeAlt } from "@fortawesome/free-solid-svg-icons";

const Classe = ({ currentStep, setClasse, classe, classes}) => {
  const [showDescriptif, setShowDescriptif] = useState(true);
  const [filteredList, setFilteredList] = useState(classes);
  const [level0spellCharac, setLevel0SpellCharac] = useState([]);
  const [level1spellCharac, setLevel1SpellCharac] = useState([]);

  const [filteredSpell, setFilteredSpell] = useState([]);

  const handleSearch = (e) => {
    let currentList = [];
    let newList = [];

    if (e.target.value !== "") {
      currentList = classes;

      newList = currentList.filter((item) => {
        const lc = item.name.toLowerCase();
        const filter = e.target.value.toLowerCase();

        return lc.includes(filter);
      });
    } else {
      newList = classes;
    }
    setFilteredList(newList);
  };

  const handleSearchSpell = (e) => {
    let currentList = [];
    let newList = [];
    if (e.target.value !== "") {
      currentList = classes;

      newList = currentList.filter((item) => {
        const lc = item.name.toLowerCase();
        const filter = e.target.value.toLowerCase();
        return lc.includes(filter);
      });
    } else if (e.target.innerHTML !== "") {
      currentList = classes;

      newList = currentList.map((item) => {
        if (item.niveau.toString() === e.target.innerHTML) {
          return item;
        }
      });
      const cleanList = newList.filter((el) => el);
      newList = cleanList;
    } else {
      newList = classes;
    }
    setFilteredSpell(newList);
  };

  const addSpell = (spell) => {
    if (spell.niveau === 0) {
      if (level0spellCharac < 3) {
        if (!level0spellCharac.includes(spell.name)) {
          setLevel0SpellCharac([...level0spellCharac, spell.name]);
        }
      }
    }
    if (spell.niveau === 1) {
      if (level1spellCharac < 2) {
        if (!level1spellCharac.includes(spell.name)) {
          setLevel1SpellCharac([...level1spellCharac, spell.name]);
        }
      }
    }
  };

  const removeSpell = (spell) => {
    if (level0spellCharac.includes(spell)) {
      setLevel0SpellCharac(level0spellCharac.filter((item) => spell !== item));
    }
    if (level1spellCharac.includes(spell)) {
      setLevel1SpellCharac(level1spellCharac.filter((item) => spell !== item));
    }
  };

  return currentStep === 2 ? (
    <Card
      style={{
        width: "80%",
        padding: 20,
        display: "block",
        margin: "0 auto",
        marginTop: "1rem",
      }}
      className="shadow"
    >
      {classe.name ? (
        <>
        <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: "1rem",
            }}
          >
            <div
              style={{flex: 1}}
            >
              <img
                alt={currentStep.name}
                src={classe.avatar}
                style={{ width: "3rem", height: "auto", padding: 3 }}
              />
              <span
                style={{
                  color: "#9C9990",
                  fontSize: "20px",
                  fontWeight: "bold",
                  lineHeight: "3rem",
                }}
              >
                {classe.name}
              </span>
              <div
                style={{
                  display: 'block',
                  margin: '0 auto',
                  width: '50%',
                  backgroundColor: '#f7f7f7',
                  padding: 5,
                  borderRadius: 5,
                  color: '#4b3b47'
                }}
              >
                <span>Point de vie de départ :{classe.pdv}</span><br />
                <span>Argent de dépard : {classe.argent}</span>
              </div>
            </div>
            <Button
              variant="danger"
              onClick={() => setClasse("classe", {})}
              style={{
                position: "absolute",
                right: "1rem",
              }}
            >
              <FontAwesomeIcon icon={faExchangeAlt} color={"#FFF"} />{' '}
              <span
                style={{
                  color: "#FFF",
                  fontWeight: "bold"
                }}
              >
                Changer de classe
              </span>
            </Button>
          </div>
          <div
            style={{
              fontSize: "15px",
              color: "#4B3B47",
              fontWeight: "bold",
              textAlign: "left",
            }}
          >
            <div
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-evenly",
                marginBottom: "1rem",
                fontWeight: "bold",
                color: "#4B3B47",
              }}
            >
              <div
                onClick={() => {
                  setShowDescriptif(true);
                }}
              >
                <Button
                  style={{
                    fontSize: "20px",
                    cursor: "pointer",
                    width: "15rem",
                  }}
                >
                  Descriptif de classe
                </Button>
              </div>
              <div
                onClick={() => {
                  setShowDescriptif(false);
                }}
              >
                <Button
                  style={{
                    fontSize: "20px",
                    cursor: "pointer",
                    width: "10rem",
                  }}
                >
                  Sorts
                </Button>
              </div>
            </div>
            <div
              style={{
                borderTop: "1px solid #9C9990",
                width: "100%",
                marginTop: "1rem",
                marginBottom: "1rem",
              }}
            />

            <a
              style={{ marginTop: "1rem" }}
              href="https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Demi-elfe.ashx"
            >
              <span>Page de description des {classe.name}</span>
            </a>

            <div
              style={{
                borderTop: "1px solid #9C9990",
                width: "100%",
                marginTop: "1rem",
              }}
            />
            {showDescriptif && classe.descriptifs ? (
              classe.descriptifs.map((descriptif) => {
                return (
                  <Accordion
                    defaultActiveKey="0"
                    style={{ marginTop: "1rem", color: "#4B3B47" }}
                  >
                    <Card>
                      <Accordion.Toggle as={Card.Header} eventKey="1">
                        {descriptif.name}
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="1">
                        <Card.Body>{descriptif.description}</Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                );
              })
            ) : (
              <>
                <Card
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    padding: 5,
                    fontSize: "15px",
                    color: "#9C9990",
                  }}
                >
                  <Form.Label
                    style={{
                      color: "#9c9990",
                      textAlign: "left",
                      fontWeight: "bold",
                    }}
                  >
                    Filtre
                  </Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Chercher un sort"
                    onChange={(e) => handleSearchSpell(e)}
                    style={{ width: "20rem" }}
                  />
                  <Form.Label
                    style={{
                      color: "#9c9990",
                      textAlign: "left",
                      fontWeight: "bold",
                    }}
                  >
                    Filtrer par niveau
                  </Form.Label>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      textAlign: "left",
                    }}
                  >
                    <div>
                      <Button
                        style={{ width: "2rem" }}
                        onClick={(e) => handleSearchSpell(e)}
                      >
                        0
                      </Button>
                    </div>
                    <div>
                      <Button
                        style={{ width: "2rem" }}
                        onClick={(e) => handleSearchSpell(e)}
                      >
                        1
                      </Button>
                    </div>
                  </div>
                </Card>

                <Accordion
                  defaultActiveKey="0"
                  style={{ marginTop: "1rem", color: "#4B3B47" }}
                >
                  <Card>
                    <Accordion.Toggle as={Card.Header} eventKey="1">
                      Sorts préparés (
                      {level0spellCharac.length + level1spellCharac.length})
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey="1">
                      <Card.Body>
                        Sorts de niveau 0 : ({level0spellCharac.length}/3)
                        <div
                          style={{ display: "flex", flexDirection: "column" }}
                        >
                          {level0spellCharac.map((spell) => (
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-around",
                              }}
                            >
                              <div>{spell}</div>
                              <div>
                                <Button
                                  style={{
                                    width: "5rem",
                                    padding: "1px",
                                    fontSize: "10px",
                                    marginLeft: "5rem",
                                  }}
                                  onClick={() => removeSpell(spell)}
                                >
                                  Désapprendre
                                </Button>
                              </div>
                            </div>
                          ))}
                        </div>
                        Sorts de niveau 1 : ({level1spellCharac.length}/2)
                        <div
                          style={{ display: "flex", flexDirection: "column" }}
                        >
                          {level1spellCharac.map((spell) => (
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "space-around",
                              }}
                            >
                              <div>{spell}</div>
                              <div>
                                <Button
                                  style={{
                                    width: "5rem",
                                    padding: "1px",
                                    fontSize: "10px",
                                    marginLeft: "5rem",
                                  }}
                                  onClick={() => removeSpell(spell)}
                                >
                                  Désapprendre
                                </Button>
                              </div>
                            </div>
                          ))}
                        </div>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
                {filteredSpell.map((sort) => {
                  return (
                    <Accordion
                      defaultActiveKey="0"
                      style={{ marginTop: "1rem", color: "#4B3B47" }}
                    >
                      <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="1">
                          {sort.name}
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                          <Card.Body>
                            {sort.description}
                            <Button
                              style={{
                                width: "5rem",
                                padding: "1px",
                                fontSize: "10px",
                                marginLeft: "5rem",
                              }}
                              onClick={() => addSpell(sort)}
                            >
                              Apprendre
                            </Button>
                          </Card.Body>
                        </Accordion.Collapse>
                      </Card>
                    </Accordion>
                  );
                })}
              </>
            )}
          </div>
        </>
      ) : (
        <>
          <Form.Label
            style={{ color: "#9c9990", textAlign: "left", fontWeight: "bold" }}
          >
            Choisis une classe
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Chercher une classe"
            onChange={(e) => handleSearch(e)}
            style={{ width: "20rem", margin: "0 auto" }}
          />
          <Row style={{ marginTop: "1rem" }}>
            {filteredList.map((c, i) => (
                <Col key={i} md={2} style={{margin: 10}} className={"card-group"}>
                  <ClasseItem
                    name={c.name}
                    avatar={c.avatar}
                    setClasse={() => setClasse("classe", c)}
                  />
                </Col>
            ))}
          </Row>
        </>
      )}
    </Card>
  ) : null;
};

export default Classe