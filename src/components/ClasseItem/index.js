import React from "react";
import { Card } from "react-bootstrap";

export default ({name, avatar, setClasse}) => (
  <Card
    variant="top"
    style={{
      width: "8rem",
      height: "10rem",
      cursor: "pointer",
      borderRadius: 10,
    }}
    onClick={setClasse}
    className={"card-hover shadow"}
  >
    <img
      className="card-img"
      alt={"classe " + name}
      src={avatar}
      style={{
        width: "100%",
        height: "100%",
        borderRadius: 10,
        objectFit: "cover",
      }}
    ></img>
    <div
      className="text-muted"
      style={{
        height: "8rem",
        borderTop: 0,
        background: "linear-gradient(rgba(0,0,0,0), #000 100%)",
        position: "absolute",
        borderRadius: 10,
        right: 0,
        bottom: 0,
        left: 0,
        padding: "0.25rem",
        paddingTop: "5rem",
      }}
    >
      <h5
        style={{
          color: "#FFF",
          marginBottom: "1rem",
        }}
      >
        {name}
      </h5>
    </div>
  </Card>
)
