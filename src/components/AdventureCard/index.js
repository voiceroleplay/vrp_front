import React from "react";
import { Card } from "react-bootstrap";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import IconButton from "../IconButton";
import { Link } from "react-router-dom";

import AdventurePlaceholder from "../../assets/adventure-placeholder.jpg"

export default ({ isAdmin, adventure }) => (
  <Link to={`/adventures/${adventure._id}`}>
    <Card
      style={{
        width: "18rem",
        height: "18rem",
        background: `url(${adventure.backgroundPicture}), url(${AdventurePlaceholder}) no-repeat`,
        backgroundSize: "cover",
        borderRadius: 10,
        margin: 10,
        padding: 0,
        cursor: "pointer"
      }}
      className="shadow mb-5 card-hover"
    >
      {isAdmin && (
        <IconButton
          style={{
            borderRadius: 50,
            right: -10,
            top: -10,
            width: 50,
            height: 50,
            position: "absolute"
          }}
          variant="warning"
          icon={faPencilAlt}
        />
      )}

      <Card.Body />
      <Card.Footer
        className="text-muted"
        style={{
          height: "5rem",
          backgroundColor: "rgba(0,0,0,0.4)",
          borderTop: 0
        }}
      >
        <h5
          style={{
            color: "#FFF",
            fontFamily: "Special Elite"
          }}
        >
          {adventure.name}
        </h5>
      </Card.Footer>
    </Card>
  </Link>
)
