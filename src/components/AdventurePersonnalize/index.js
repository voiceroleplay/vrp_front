import React, { useState } from "react";
import { Card } from "react-bootstrap";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Switch from "react-switch";

const AdventurePersonnalize = ({ isAdmin, adventure }) => {
  const [isChecked, setIsChecked] = useState(false);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        textAlign: "left",
        margin: 10
      }}
    >
      <div style={{ width: "12rem" }}>
        <span
          style={{
            color: "#80757d",
            fontSize: "16px",
            textAlign: "left",
            // paddingLeft: "1rem"
          }}
        >
          CARTE PERSONNALISEE
        </span>
        <div style={{ position: "relative", left: "60px", top: "3px" }}>
          <Switch
            activeBoxShadow="0 0 2px 3px #3bf"
            offHandleColor="#F99494"
            onHandleColor="#F99494"
            onColor="#fff"
            offColor="#80757D"
            width={70}
            height={40}
            checkedIcon={false}
            uncheckedIcon={false}
            onChange={() => (isChecked === true ? false : true)}
            checked={isChecked}
            id="normal-switch"
          />
        </div>
      </div>
      <div>
        <Card
          style={{
            width: "11rem",
            height: "10rem",
            marginTop: "2.5rem",
            background: "#4B3B47",
            cursor: "pointer"
          }}
          className="shadow card-hover"
          onClick={() => alert("Personnages")}
        >
          <FontAwesomeIcon
            size="3x"
            icon={faBook}
            style={{ color: "white", margin: "1rem auto 0 auto" }}
          />

          <Card.Footer
            className="text-muted"
            style={{
              height: "5rem",
              borderTop: 0
            }}
          >
            <h5
              style={{
                color: "#FFF",
                margin: "auto",
                textAlign: "center"
              }}
            >
              Histoires
            </h5>
          </Card.Footer>
        </Card>
      </div>
    </div>
  );
};

export default AdventurePersonnalize;
