import React from "react"
import { Modal } from "react-bootstrap"

export default ({
  show,
  handleClose
}) => (
  <Modal
    show={show}
    onHide={handleClose}
  >
    <img
      src={require('../../assets/easter-egg.png')}
      alt="Easter Egg"
      style={{objectFit: 'cover'}}
    />
    <p
      style={{
        position: 'absolute',
        color: '#FFF',
        fontSize: 30,
        textAlign: 'center',
        fontFamily: 'AbeeZee',
        marginTop: 20
      }}
    >You found the treasure room ! It won't give you anything tho ...</p>
  </Modal>
);