import React from 'react'
import { Modal, ModalBody, Button, Spinner } from 'react-bootstrap'
import './styles.css'
import Dice from '../Dice'

export default ({
  show,
  handleClose,
  rollDices,
  rolling,
  dicesTotal,
  dicesValue,
  selectedCharacteristic
}) => (
  <Modal
    show={show}
    onHide={handleClose}
  >
    <ModalBody>
      {selectedCharacteristic.value === -1 ? (
        <div>
          <h4 style={{textAlign: 'center'}}>{selectedCharacteristic.id}</h4>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              margin: 30
            }}
          >
            {[1, 2, 3].map((i) => (
              <Dice rolling={rolling} />
            ))}
          </div>
          {rolling && (
            <div style={{
              margin: 30,
              marginBottom: 50
            }}>
              <Spinner animation="border" style={{margin: '0 auto', display: 'block'}}/>
            </div>
          )}
          {!rolling && (
            <Button
              onClick={() => rollDices(selectedCharacteristic)}
              style={{
                display: 'block',
                margin: '0 auto'
              }}
            >
              Lancer les dès
            </Button>
          )}
        </div>
      ) : (
        <div>
          <h4 style={{textAlign: 'center'}}>{selectedCharacteristic.id}</h4>
          <div style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            margin: 30,
            marginBottom: 50
          }}>
            {dicesValue.map((d, index) => (
              <p className={'dice-value'}>{d}</p>
            ))}
          </div>
          <p className={'dice-value-big'}>
            {dicesTotal !== -1 ? dicesTotal : selectedCharacteristic.value}
          </p>
          <span className={'dice-label'}>TOTAL</span>
        </div>
      )}
    </ModalBody>
  </Modal>
)