import React, { useState } from "react";
import { Table, Row, Col, Button } from "react-bootstrap";
import { faDiceD20 } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import DiceRandom from "../DiceRandom";
import force from "../../assets/force.png";
import dexterity from "../../assets/dexterite.png";
import constitution from "../../assets/constitution.png";
import intelligence from "../../assets/intelligence.png";
import charisma from "../../assets/charisme.png";
import wisdom from "../../assets/sagesse.png";

const TableItem = ({ id, title, icon, ligns, dice, handleShow }) => (
  <Col xs lg="2">
    {dice ? (
      <p>{dice}</p>
    ) : (
      <Button style={{backgroundColor: "transparent"}} onClick={() => handleShow(id)}>
        <FontAwesomeIcon
          icon={faDiceD20}
          style={{ color: "#4B3B47", margin: 5 }}
        />
        <p style={{ display: "inline-block", marginBottom: 0, color: "#4B3B47" }}>
          Lancer les dès
        </p>
      </Button>
    )}
    <Table striped bordered hover size="sm">
      <thead style={{ backgroundColor: "#4B3B47" }}>
        <tr>
          <th colSpan={2} style={{ color: "#FFF" }}>
            {icon && (
              <img
                src={icon}
                style={{ color: "#FFF", width: "5rem", height: "auto" }}
              />
            )}
            {icon && <br />}
            {title}
          </th>
        </tr>
      </thead>
      <tbody>
        {ligns.map((l) => (
          <tr key={l.id}>
            <td>{l.name}</td>
            <td>{l.value}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  </Col>
);

export default ({ currentStep, characteristics, setCharacteristic }) => {
  const [show, setShow] = useState(false);
  const [rolling, setRolling] = useState(false);
  const [dicesValue, setDicesValue] = useState([]);
  const [dicesTotal, setDicesTotal] = useState(0);
  const [selectedCharacteristic, setSelectedCharacteristic] = useState({});

  const handleClose = () => setShow(false);
  const handleShow = (id) => {
    console.log(characteristics);
    console.log("CHARACTERISTIC", characteristics[id]);
    setDicesValue([]);
    setDicesTotal(-1);
    setSelectedCharacteristic(characteristics[id]);
    setShow(true);
  };

  const rollDices = ({ id, value }) => {
    setRolling(true);
    setTimeout(function () {
      const number1 = Math.floor(Math.random() * 6) + 1;
      const number2 = Math.floor(Math.random() * 6) + 1;
      const number3 = Math.floor(Math.random() * 6) + 1;
      const total = [number1, number2, number3].reduce((a, b) => a + b, 0);
      setRolling(false);
      setDicesValue([number1, number2, number3]);
      setDicesTotal(total);
      console.log("NAME", id);
      let newCharacteristics = characteristics;
      newCharacteristics[id].value = total;
      setCharacteristic("characteristics", newCharacteristics);
    }, 3000);
  };

  const getMod = (value) => {
    console.log("ok");
    if (value === 3) {
      return "-4";
    }
    if (value >= 4 && value <= 5) {
      return "-3";
    }
    if (value >= 6 && value <= 7) {
      return "-2";
    }
    if (value >= 8 && value <= 9) {
      return "-1";
    }
    if (value >= 10 && value <= 11) {
      return "0";
    }
    if (value >= 12 && value <= 13) {
      return "+1";
    }
    if (value >= 14 && value <= 15) {
      return "+2";
    }
    if (value >= 16 && value <= 17) {
      return "+3";
    }
    if (value === 18) {
      return "+4";
    }
  };

  return currentStep === 3 ? (
    <div>
      <DiceRandom
        show={show}
        handleClose={handleClose}
        rollDices={rollDices}
        rolling={rolling}
        dicesValue={dicesValue}
        dicesTotal={dicesTotal}
        selectedCharacteristic={selectedCharacteristic}
      />
      <Row
        style={{
          backgroundColor: "#FFF",
          padding: 10,
          marginLeft: 20,
          marginRight: 20,
        }}
        className={"shadow justify-content-md-center"}
      >
        <TableItem
          id={"strength"}
          handleShow={handleShow}
          title={"Force"}
          icon={force}
          ligns={[
            {
              id: "value",
              name: "Valeur",
              value:
                characteristics.strength.value !== -1
                  ? characteristics.strength.value
                  : 0,
            },
            {
              id: "mod",
              name: "Mod",
              value:
                characteristics.strength.value !== -1
                  ? getMod(characteristics.strength.value)
                  : 0,
            },
            {
              id: "base",
              name: "Base",
              value:
                characteristics.strength.value !== -1
                  ? characteristics.strength.value
                  : 0,
            },
            { id: "Race", name: "Race", value: "0" },
            { id: "alter", name: "Altér", value: "0" },
          ]}
        />
        <TableItem
          id={"dexterity"}
          handleShow={handleShow}
          title={"Dextérité"}
          icon={dexterity}
          ligns={[
            {
              id: "value",
              name: "Valeur",
              value:
                characteristics.dexterity.value !== -1
                  ? characteristics.dexterity.value
                  : 0,
            },
            {
              id: "mod",
              name: "Mod",
              value:
                characteristics.dexterity.value !== -1
                  ? getMod(characteristics.dexterity.value)
                  : 0,
            },
            {
              id: "base",
              name: "Base",
              value:
                characteristics.dexterity.value !== -1
                  ? characteristics.dexterity.value
                  : 0,
            },
            { id: "Race", name: "Race", value: "0" },
            { id: "alter", name: "Altér", value: "0" },
          ]}
        />
        <TableItem
          id={"constitution"}
          handleShow={handleShow}
          title={"Constitution"}
          icon={constitution}
          ligns={[
            {
              id: "value",
              name: "Valeur",
              value:
                characteristics.constitution.value !== -1
                  ? characteristics.constitution.value
                  : 0,
            },
            {
              id: "mod",
              name: "Mod",
              value:
                characteristics.constitution.value !== -1
                  ? getMod(characteristics.constitution.value)
                  : 0,
            },
            {
              id: "base",
              name: "Base",
              value:
                characteristics.constitution.value !== -1
                  ? characteristics.constitution.value
                  : 0,
            },
            { id: "Race", name: "Race", value: "0" },
            { id: "alter", name: "Altér", value: "0" },
          ]}
        />
        <TableItem
          id={"intelligence"}
          handleShow={handleShow}
          title={"Intelligence"}
          icon={intelligence}
          ligns={[
            {
              id: "value",
              name: "Valeur",
              value:
                characteristics.intelligence.value !== -1
                  ? characteristics.intelligence.value
                  : 0,
            },
            {
              id: "mod",
              name: "Mod",
              value:
                characteristics.intelligence.value !== -1
                  ? getMod(characteristics.intelligence.value)
                  : 0,
            },
            {
              id: "base",
              name: "Base",
              value:
                characteristics.intelligence.value !== -1
                  ? characteristics.intelligence.value
                  : 0,
            },
            { id: "Race", name: "Race", value: "0" },
            { id: "alter", name: "Altér", value: "0" },
          ]}
        />
        <TableItem
          id={"wisdom"}
          handleShow={handleShow}
          title={"Sagesse"}
          icon={wisdom}
          ligns={[
            {
              id: "value",
              name: "Valeur",
              value:
                characteristics.wisdom.value !== -1
                  ? characteristics.wisdom.value
                  : 0,
            },
            {
              id: "mod",
              name: "Mod",
              value:
                characteristics.wisdom.value !== -1
                  ? getMod(characteristics.wisdom.value)
                  : 0,
            },
            {
              id: "base",
              name: "Base",
              value:
                characteristics.wisdom.value !== -1
                  ? characteristics.wisdom.value
                  : 0,
            },
            { id: "Race", name: "Race", value: "0" },
            { id: "alter", name: "Altér", value: "0" },
          ]}
        />
        <TableItem
          id={"charisma"}
          handleShow={handleShow}
          title={"Charisme"}
          icon={charisma}
          ligns={[
            {
              id: "value",
              name: "Valeur",
              value:
                characteristics.charisma.value !== -1
                  ? characteristics.charisma.value
                  : 0,
            },
            {
              id: "mod",
              name: "Mod",
              value:
                characteristics.charisma.value !== -1
                  ? getMod(characteristics.charisma.value)
                  : 0,
            },
            {
              id: "base",
              name: "Base",
              value:
                characteristics.charisma.value !== -1
                  ? characteristics.charisma.value
                  : 0,
            },
            { id: "Race", name: "Race", value: "0" },
            { id: "alter", name: "Altér", value: "0" },
          ]}
        />
      </Row>
    </div>
  ) : null;
};
