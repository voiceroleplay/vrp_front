import React from "react";
import { Card } from "react-bootstrap";
import sack from "../../assets/pouch.png";

export default ({ personnage, index }) => (
  <Card
    style={{
      borderRadius: 10,
      width: "330px",
      height: "330px",
      padding: 0,
      cursor: "pointer",
      marginBottom: "2rem",
      marginTop: "1rem",
      border: "1px dashed #979797"
    }}
  >
    <img
      alt={personnage.name}
      className="card-img"
      src={sack}
      style={{
        width: "200px",
        marginTop: "50px",
        height: "auto",
        opacity: "10%"
      }}
    ></img>
    <Card.Body
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        marginTop: "-120px"
      }}
    >
      <p
        style={{
          fontFamily: "Special Elite",
          color: "#979797",
          fontSize: 18,
          margin: 10
        }}
      >
        Personnage <br></br>
        {personnage.length + 2 + index} / 6
      </p>
    </Card.Body>
  </Card>
);
