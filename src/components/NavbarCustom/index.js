import React from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faUserCircle } from "@fortawesome/free-solid-svg-icons";

import Logo from "../../assets/logo-voiceroleplay-light.svg";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getAdventures } from "../../redux/actions/adventure.actions";
import { userActions } from "../../redux/actions/user.actions";

class NavbarCustom extends React.Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  render(){
    const { title, adventures, logout, currentAdventure } = this.props
    const { user } = this.props.user
    return(
      <Navbar bg="white" variant="light" expand="lg" fixed="top" className="shadow">
        <Navbar.Brand>
          <Link to={'/'}>
            <img
              src={Logo}
              width="40"
              height="40"
              className="d-inline-block align-top"
              alt="Voice Role Play Logo"
            />
            {title}
          </Link>
        </Navbar.Brand>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Link className={`nav-link${window.location.pathname === "/" ? " active" : ""}`} to="/"><h6>Aventures</h6></Link>
            <Link className={`nav-link${window.location.pathname === "/characters" ? " active" : ""}`} to="/characters"><h6>Mes personnages</h6></Link>
          </Nav>
        </Navbar.Collapse>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          {currentAdventure ? (
            <Nav
              className="mr-auto"
              style={{ background: "#4B3B47", height: "4.5rem", margin: "-10px" }}
            >
              <Nav.Link href="/">
                <h3
                  style={{
                    color: "white",
                    marginTop: "15px",
                    padding: "0 2rem 0 2rem",
                    fontFamily: "Special Elite"
                  }}
                >
                  {currentAdventure}
                </h3>
              </Nav.Link>
            </Nav>
          ) : null}
          <Link className={`nav-link${window.location.pathname === "/profile" ? " active" : ""}`} to="/profile">
            <div style={{ display: "inline-block", marginRight: 10 }}>
              <p style={{ margin: 0, fontWeight: "bold" }}>
                {user.displayName ? user.displayName : user.email.split("@")[0]}
              </p>
              <p style={{ margin: 0 }}>{adventures ? adventures.length : 0} aventures</p>
            </div>
            <div style={{ display: "inline-block", marginRight: 10 }}>
              {user.photoUrl ? (
                <img alt="" src={user.photoUrl} width="40" height="40" />
              ) : (
                <FontAwesomeIcon size={"2x"} icon={faUserCircle} style={{ color: "#000" }} />
              )}
            </div>
          </Link>
          <NavDropdown alignRight title="" id="nav-dropdown">
            <NavDropdown.Item onClick={logout}>Se déconnecter</NavDropdown.Item>
          </NavDropdown>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

const mapState = (state) => {
  const { loggingIn, user } = state.authentication;
  const { adventures } = state.adventures;
  return { loggingIn, user, adventures };
};

const mapDispatchToProps = {
  logout: userActions.logout,
  getAdventures: getAdventures,
};

export default connect(mapState, mapDispatchToProps)(NavbarCustom);
