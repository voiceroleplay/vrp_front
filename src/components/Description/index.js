import React from "react";
import { Accordion, Card, Row, Col } from "react-bootstrap";
import { Form } from "react-bootstrap";

const Description = (currentStep) => {
  const activeStep = currentStep.currentStep;
  return activeStep === 4 ? (
    <Card
      style={{
        width: "80%",
        padding: 20,
        display: "block",
        margin: "0 auto",
        marginTop: "1rem",
        fontSize: "15px",
        color: "#4B3B47",
        fontWeight: "bold",
        textAlign: "left",
      }}
      className="shadow"
    >
      <Row>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Age</Form.Label>
            <Form.Control type="text" placeholder="30" />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Taille</Form.Label>
            <Form.Control type="text" placeholder="1m75" />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Poids</Form.Label>
            <Form.Control type="text" placeholder="80" />
          </Form.Group>
        </Col>
      </Row>     
      <Row>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Couleur de la peau</Form.Label>
            <Form.Control type="text" placeholder="Mate" />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Couleur des yeux</Form.Label>
            <Form.Control type="text" placeholder="Marrons" />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Cheveux</Form.Label>
            <Form.Control type="text" placeholder="Bruns longs" />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Croyance</Form.Label>
            <Form.Control type="text" placeholder="Abadar" />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Alignement</Form.Label>
            <Form.Control as="select">
              <option>- Choisir alignement -</option>
              <option>Loyal Bon</option>
              <option>Loyal Mauvais</option>
              <option>Neutre</option>
              <option>Chaotique Bon</option>
              <option>Chaotique Mauvais</option>
            </Form.Control>
          </Form.Group>
        </Col>
        </Row>
      <Row>
        <Col>
          <Form.Group controlId="exampleForm.ControlSelect1">
            <Form.Label>Histoire</Form.Label>
            <Form.Control as="textarea" rows="3" />
          </Form.Group>
        </Col>
      </Row>
    </Card>
  ) : null;
};

export default Description;
