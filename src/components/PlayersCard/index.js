import React from "react";
import { Card } from "react-bootstrap";
import { Form } from "react-bootstrap";
import { faStreetView } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default ({ isAdmin, adventure }) => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      textAlign: "left",
      margin: 10
    }}
  >
    <div>
      <span
        style={{
          color: "#80757d",
          fontSize: "16px",
          textAlign: "left",
          paddingLeft: "1rem"
        }}
      >
        NB DE JOUEURS
      </span>

      <Form.Control
        as="select"
        style={{
          marginLeft: "15px",
          color: "#4B3B47",
          fontWeight: "bold",
          fontSize: "20px",
          padding: "0.5rem 1rem 0.5rem 1rem",
          cursor: "pointer",
          width: "8rem"
        }}
        className="shadow"
      >
        <option>1 - 18</option>
        <option>...</option>
      </Form.Control>
    </div>
    <div>
      <Card
        style={{
          width: "11rem",
          height: "10rem",
          marginTop: "2.7rem",
          background: "#4B3B47",
          cursor: "pointer"
        }}
        className="shadow card-hover"
        onClick={() => alert("Personnages")}
      >
        <FontAwesomeIcon
          size="3x"
          icon={faStreetView}
          style={{ color: "white", margin: "1rem auto 0 auto" }}
        />

        <Card.Footer
          className="text-muted"
          style={{
            height: "5rem",
            borderTop: 0
          }}
        >
          <h5
            style={{
              color: "#FFF",
              margin: "auto",
              textAlign: "center"
            }}
          >
            Personnages
          </h5>
        </Card.Footer>
      </Card>
    </div>

    {console.log(adventure)}
  </div>
);
