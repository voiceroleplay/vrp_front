import React from "react";
import { Card } from "react-bootstrap";

export default ({ isAdmin, adventure }) => (
  <div
    style={{
      display: "flex",
      flexDirection: "column",
      textAlign: "left",
      margin: 10,
      width: "18rem"
    }}
  >
    <div>
      <span
        style={{
          color: "#80757d",
          fontSize: "16px",
          textAlign: "left",
          paddingLeft: "1rem"
        }}
      >
        TITRE
      </span>
      <Card
        style={{
          width: "20rem",
          maxWidth: "300px",
          maxHeight: "3rem",
          overflow: "auto"
        }}
        className="shadow"
      >
        <Card.Body
          style={{
            color: "#4B3B47",
            fontWeight: "bold",
            fontSize: "20px",
            padding: "0.5rem 1rem 0.5rem 1rem",
            textAlign: "left"
          }}
        >
          {adventure.name}
        </Card.Body>
      </Card>
    </div>
    <div>
      <span
        style={{
          color: "#80757d",
          fontSize: "16px",
          textAlign: "left",
          paddingLeft: "1rem"
        }}
      >
        RESUME
      </span>
      <Card
        style={{
          width: "20rem",
          maxWidth: "300px",
          height: "10rem",
          overflow: "auto"
        }}
        className="shadow"
      >
        <Card.Body
          style={{
            color: "#4B3B47",
            fontSize: "15px",
            padding: "0.5rem 1rem 0.5rem 1rem",
            textAlign: "left"
          }}
        >
          {adventure.description}
        </Card.Body>
      </Card>
    </div>

    {console.log(adventure)}
  </div>
);
