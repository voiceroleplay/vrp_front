import React from "react";
import './styles.css'

export default ({rolling}) => (
  <div className={`dice-container${rolling ? " add-keyframe" : ""}`}>
    <div className="dice">
      <div className="front">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div className="back">
        <span></span>
      </div>
      <div className="right">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div className="left">
        <span></span>
        <span></span>
      </div>
      <div className="top">
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div className="bottom">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
);
