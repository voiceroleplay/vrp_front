import React, { useState } from "react";
import { Accordion, Card } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Form } from "react-bootstrap";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import equipementData from "../../mocks/equipements.json";

import { faCoins, faDiceD20 } from "@fortawesome/free-solid-svg-icons";
import DiceRandom from "../DiceRandom";

const Equipements = (currentStep) => {
  const activeStep = currentStep.currentStep;
  const [filteredEquipement, setFilteredEquipement] = useState(equipementData);
  const [characEquipement, setCharacEquipement] = useState([]);
  const [money, setMoney] = useState(-1)
  const [show, setShow] = useState(false);
  const [rolling, setRolling] = useState(false);
  const [dicesValue, setDicesValue] = useState([]);
  const [dicesTotal, setDicesTotal] = useState(0);

  const handleClose = () => setShow(false);
  const handleShow = (id) => {
    setDicesValue([]);
    setDicesTotal(-1);
    setShow(true);
  };

  const rollDices = ({ id, value }) => {
    setRolling(true);
    setTimeout(function () {
      const number1 = Math.floor(Math.random() * 6) + 1;
      const number2 = Math.floor(Math.random() * 6) + 1;
      const number3 = Math.floor(Math.random() * 6) + 1;
      const total = [number1, number2, number3].reduce((a, b) => a + b, 0);
      setRolling(false);
      setDicesValue([number1, number2, number3]);
      setDicesTotal(total);
      setMoney(total)
    }, 3000);
  };

  const handleSearch = (e) => {
    let currentList = [];
    let newList = [];
    if (e.target.value !== "") {
      currentList = equipementData;

      newList = currentList.filter((item) => {
        const lc = item.name.toLowerCase();
        const filter = e.target.value.toLowerCase();
        return lc.includes(filter);
      });
    } else if (e.target.innerHTML !== "") {
      currentList = equipementData;

      newList = currentList.map((item) => {
        if (item.type.includes(e.target.innerHTML.toLowerCase())) {
          return item;
        }
      });
      const cleanList = newList.filter((el) => el);
      newList = cleanList;
    } else {
      newList = equipementData;
    }
    setFilteredEquipement(newList);
  };

  const addEquipment = (equipement) => {
    setCharacEquipement([...characEquipement, equipement]);
  };

  const removeEquipement = (equipement) => {
    setCharacEquipement(characEquipement.filter((item) => equipement !== item));
  };
  return activeStep === 5 ? (
    <>
      <Card
        style={{
          width: "80%",
          padding: 20,
          display: "block",
          margin: "0 auto",
          marginTop: "1rem",
          textAlign: "left",
          fontSize: "15px",
        }}
      >
        <div
          style={{
            position: "absolute",
            right: 20,
          }}
        >
          {money !== -1 ? (
            <div
              style={{ fontSize: "15px", color: "#4B3B47", fontWeight: "bold" }}
            >
              <FontAwesomeIcon
                icon={faCoins}
                size={"3x"}
                style={{display: 'block', margin: '0 auto'}}
              />
              <p style={{textAlign: 'center'}}>{money}€</p>
              <p style={{textAlign: 'center'}}>Argent de départ</p>
            </div>
          ) : (
            <div>
              <DiceRandom
                show={show}
                handleClose={handleClose}
                rollDices={rollDices}
                rolling={rolling}
                dicesValue={dicesValue}
                dicesTotal={dicesTotal}
                selectedCharacteristic={{ id: "Monnaie", value: money }}
              />
              <Button
                style={{backgroundColor: "transparent"}}
                onClick={handleShow}
              >
                <FontAwesomeIcon
                  icon={faDiceD20}
                  style={{ color: "#4B3B47", margin: 5 }}
                  size={"2x"}
                />
                <p style={{ marginBottom: 0, color: "#4B3B47" }}>
                  Générer l'argent de départ
                </p>
              </Button>
            </div>
          )}
        </div>
        <Form.Label
          style={{
            color: "#9c9990",
            textAlign: "left",
            fontWeight: "bold",
          }}
        >
          Filtre
        </Form.Label>
        <Form.Control
          type="text"
          placeholder="Chercher un sort"
          style={{ width: "20rem" }}
          onChange={(e) => handleSearch(e)}
        />
        <Form.Label
          style={{
            color: "#9c9990",
            textAlign: "left",
            fontWeight: "bold",
          }}
        >
          Filtrer par niveau
        </Form.Label>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            textAlign: "left",
          }}
        >
          <div>
            <Button style={{ width: "6rem" }} onClick={(e) => handleSearch(e)}>
              Armure
            </Button>
          </div>
          <div>
            <Button style={{ width: "5rem" }} onClick={(e) => handleSearch(e)}>
              Arme
            </Button>
          </div>
        </div>
        <Accordion
          defaultActiveKey="0"
          style={{ marginTop: "1rem", color: "#4B3B47" }}
        >
          <Card style={{ fontWeight: "bold" }}>
            <Accordion.Toggle as={Card.Header} eventKey="1">
              Inventaire ({characEquipement.length})
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="1">
              <Card.Body>
                {characEquipement.map((item) => (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-around",
                    }}
                  >
                    <div>{item.name}</div>
                    <div>Bonus d'armure : {item.armureBonus}</div>
                    <div>Bonus de dextérité max : {item.maxDexBonus}</div>
                    <div>Malus d'amure aux tests : {item.armureMalus}</div>
                    <div>
                      <Button
                        style={{
                          width: "5rem",
                          padding: "1px",
                          fontSize: "10px",
                        }}
                        onClick={() => removeEquipement(item)}
                      >
                        Vendre
                      </Button>
                    </div>
                  </div>
                ))}
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
        {filteredEquipement.map((item, i) => {
          return (
            <Accordion
              key={i}
              defaultActiveKey="0"
              style={{ marginTop: "1rem", color: "#4B3B47" }}
            >
              <Card style={{ fontWeight: "bold" }}>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                  {item.name}
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                  <Card.Body>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "space-around",
                      }}
                    >
                      <div>Bonus d'armure : {item.armureBonus}</div>
                      <div>Bonus de dextérité max : {item.maxDexBonus}</div>
                      <div>Malus d'amure aux tests : {item.armureMalus}</div>
                      <div>
                        <Button
                          style={{
                            width: "5rem",
                            padding: "1px",
                            fontSize: "10px",
                          }}
                          onClick={() => addEquipment(item)}
                        >
                          Acheter
                        </Button>
                      </div>
                    </div>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          );
        })}
      </Card>
    </>
  ) : null;
};

export default Equipements;
