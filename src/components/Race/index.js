import React, { useState } from "react";
import { Card, Form, CardColumns, Accordion, Button, CardDeck, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExchangeAlt } from "@fortawesome/free-solid-svg-icons";

const Race = ({ currentStep, setRace, race, races }) => {
  const [filteredList, setFilteredList] = useState(races);
  const traitLength = race.traits ? race.traits.length : null;

  const handleSearch = (e) => {
    let currentList = [];
    let newList = [];

    if (e.target.value !== "") {
      currentList = races;

      newList = currentList.filter((item) => {
        const lc = item.name.toLowerCase();
        const filter = e.target.value.toLowerCase();

        return lc.includes(filter);
      });
    } else {
      newList = races;
    }
    setFilteredList(newList);
  };

  return currentStep === 1 ? (
    <Card
      style={{
        width: "80%",
        padding: 20,
        display: "block",
        margin: "0 auto",
        marginTop: "1rem",
      }}
      className="shadow"
    >
      {race.name ? (
        <>
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              marginBottom: "1rem",
            }}
          >
            <div style={{ flex: 1 }}>
              <span
                style={{
                  color: "#9C9990",
                  fontSize: "20px",
                  fontWeight: "bold",
                  lineHeight: "3rem",
                }}
              >
                {race.name}
              </span>
            </div>
            <Button
              variant="danger"
              onClick={() => setRace("race", {})}
              style={{
                position: "absolute",
                right: "1rem",
              }}
            >
              <FontAwesomeIcon icon={faExchangeAlt} color={"#FFF"} />{" "}
              <span
                style={{
                  color: "#FFF",
                  fontWeight: "bold",
                }}
              >
                Changer de race
              </span>
            </Button>
          </div>
          <div
            style={{
              color: "#9C9990",
              fontSize: "15px",
              fontWeight: "bold",
              textAlign: "left",
            }}
          >
            {race.description}
          </div>
          <div
            style={{
              color: "#9C9990",
              fontSize: "15px",
              fontWeight: "bold",
              marginTop: "1rem",
              textAlign: "left",
            }}
          >
            <p style={{ fontSize: "20px", textAlign: "left" }}>
              Traits raciaux
            </p>
            {race.traits &&
              race.traits.map((trait, index) =>
                index !== traitLength ? trait.title + ", " : trait.title + "."
              )}

            <div
              style={{
                borderTop: "1px solid #9C9990",
                width: "100%",
                marginTop: "1rem",
                marginBottom: "1rem",
              }}
            />

            <a
              style={{ marginTop: "1rem" }}
              href="https://www.pathfinder-fr.org/Wiki/Pathfinder-RPG.Demi-elfe.ashx"
            >
              <span>Page de description des {race.name}</span>
            </a>

            <div
              style={{
                borderTop: "1px solid #9C9990",
                width: "100%",
                marginTop: "1rem",
              }}
            />

            {race.traits &&
              race.traits.map((trait, index) => {
                return (
                  <Accordion
                    key={index}
                    defaultActiveKey="0"
                    style={{ marginTop: "1rem", color: "#4B3B47" }}
                  >
                    <Card>
                      <Accordion.Toggle as={Card.Header} eventKey="1">
                        {trait.title}
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="1">
                        <Card.Body>{trait.description}</Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                );
              })}
          </div>
        </>
      ) : (
        <>
          <Form.Label
            style={{ color: "#9c9990", textAlign: "left", fontWeight: "bold" }}
          >
            Choisis une race
          </Form.Label>
          <Form.Control
            type="text"
            placeholder="Chercher une race"
            onChange={(e) => handleSearch(e)}
            style={{ width: "20rem", margin: "0 auto" }}
          />
          <Row
            style={{
              marginTop: "1rem",
            }}
          >
            {filteredList.map((r, index) => {
              return (
                <Col key={index} md={2} style={{margin: 10}} className={"card-group"}>
                  <Card
                    variant="top"
                    style={{
                      width: "8rem",
                      height: "10rem",
                      cursor: "pointer",
                      borderRadius: 10,
                    }}
                    onClick={() => setRace("race", r)}
                    className={"card-hover shadow"}
                  >
                    <img
                      className="card-img"
                      alt={"race " + r.name}
                      src={r.avatar}
                      style={{
                        width: "100%",
                        height: "100%",
                        borderRadius: 10,
                        objectFit: "cover",
                      }}
                    ></img>
                    <div
                      className="text-muted"
                      style={{
                        height: "8rem",
                        borderTop: 0,
                        background: "linear-gradient(rgba(0,0,0,0), #000 100%)",
                        position: "absolute",
                        borderRadius: 10,
                        right: 0,
                        bottom: 0,
                        left: 0,
                        padding: "0.25rem",
                        paddingTop: "5rem",
                      }}
                    >
                      <h5
                        style={{
                          color: "#FFF",
                          marginBottom: "1rem",
                        }}
                      >
                        {r.name}
                      </h5>
                    </div>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </>
      )}
    </Card>
  ) : null;
};

export default Race;
