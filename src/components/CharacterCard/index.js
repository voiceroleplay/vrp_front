import React from "react";
import { Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt, faTimes } from "@fortawesome/free-solid-svg-icons";

export default ({ personnage }) => (
  <Card
    style={{
      borderRadius: 10,
      width: "330px",
      height: "330px",
      padding: 0,
      cursor: "pointer",
      marginBottom: "2rem",
      marginTop: "1rem"
    }}
    className="shadow card-hover"
  >
    <>
      <FontAwesomeIcon
        style={{
          borderRadius: 50,
          right: -10,
          top: -15,
          width: 40,
          height: 40,
          padding: 10,
          position: "absolute",
          background: "#F99494",
          color: "#4B3B47"
        }}
        size="2x"
        className="shadow"
        icon={faTimes}
        onClick={() => alert("delete " + personnage.name)}
      />
      <FontAwesomeIcon
        style={{
          borderRadius: 50,
          right: 35,
          top: -15,
          width: 40,
          height: 40,
          padding: 10,
          position: "absolute",
          background: "#fff",
          color: "#4B3B47"
        }}
        icon={faPencilAlt}
        className="shadow"
        onClick={() => alert("edit " + personnage.name)}
      />
    </>

    <img
      alt={personnage.name}
      className="card-img"
      src={personnage.avatar}
      style={{
        width: "100%",
        height: "100%",
        borderRadius: "calc(.25rem - 1px)"
      }}
    ></img>
    <div
      className="text-muted"
      style={{
        height: "10rem",
        borderTop: 0,
        background: "linear-gradient(rgba(0,0,0,0), #000 100%)",
        position: "absolute",
        top: 170,
        borderRadius: 10,
        right: 0,
        bottom: 0,
        left: 0,
        padding: "1.25rem"
      }}
    >
      <h2
        style={{
          color: "#FFF",
          marginBottom: "1rem"
        }}
      >
        {personnage.name}
      </h2>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          color: "#FFF",
          width: "18rem",
          justifyContent: "space-between"
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between"
          }}
        >
          <div style={{ marginBottom: "1rem" }}>
            <h4>{personnage.level}</h4>
          </div>
          <div style={{ fontSize: "20px" }}>Niveau</div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between"
          }}
        >
          <div>
            <h4>{personnage.race}</h4>
          </div>
          <div style={{ fontSize: "20px" }}>Race</div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between"
          }}
        >
          <div>
            <h4>{personnage.class}</h4>
          </div>
          <div style={{ fontSize: "20px" }}>Classe</div>
        </div>
      </div>
    </div>
  </Card>
);
