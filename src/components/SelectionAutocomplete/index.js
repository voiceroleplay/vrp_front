import React from "react"
import { Typeahead } from "react-bootstrap-typeahead"

import 'react-bootstrap-typeahead/css/Typeahead.css';

export default ({name, data, multiple, message, renderMenuItemChildren, onChange}) => (
    <Typeahead
        clearButton
        id={`${name}-selection`}
        labelKey={name}
        multiple={multiple}
        options={data}
        placeholder={message}
        renderMenuItemChildren={renderMenuItemChildren}
        onChange={onChange}
        style={{
            backgroundColor: "#E5E5E5"
        }}
    />
)