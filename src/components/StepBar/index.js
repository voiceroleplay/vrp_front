import React from "react";
import { Button } from "react-bootstrap";

export default ({ steps, activeStep, handleChangeActiveStep }) => {
  return (
    <div
      style={{
        display: "inline-flex",
      }}
    >
      {steps.map((step, index) => {
        return (
          <Button
            key={index}
            variant={step.id === activeStep ? "info" : "light"}
            onClick={() => handleChangeActiveStep("currentStep", step.id)}
            style={{
              width: 160,
              margin: 5,
            }}
          >
            {step.name}
          </Button>
        );
      })}
    </div>
  );
};
