import React, { useState } from "react";
import PropTypes from "prop-types";
import SpeechRecognition from "react-speech-recognition";
import IconButton from "../IconButton";
import { faMicrophone } from "@fortawesome/free-solid-svg-icons";
import { Alert, Container, Row, Col } from "react-bootstrap";

const propTypes = {
  // Props injected by SpeechRecognition
  transcript: PropTypes.string,
  resetTranscript: PropTypes.func,
  browserSupportsSpeechRecognition: PropTypes.bool,
  startListening: PropTypes.func,
  stopListening: PropTypes.func
};

const Dictaphone = ({
  transcript,
  resetTranscript,
  browserSupportsSpeechRecognition,
  startListening,
  stopListening
}) => {
  const [listening, setListening] = useState(false)

  const handleChangeListening = () => {
    setListening(!listening)
    console.log('LISTENING', listening)
    if (!listening === true) startListening()
    else stopListening()
  }

  if (!browserSupportsSpeechRecognition) {
    return null;
  }

  if (transcript) {
    console.log('TRANSCRIPT', transcript)
  }

  return (
      <Container style={{position: 'absolute'}}>
        <Row className="justify-content-md-end">
          <Col xs lg="4">
            {transcript && (
              <p
                style={{
                  backgroundColor: '#5a5a5a',
                  color: '#FFF',
                  textAlign: 'right',
                  borderRadius: '10px 10px 0px 10px',
                  padding: 10,
                  transition: 'all 500ms ease'
                }}
                className={'shadow'}
              >
                {transcript}
              </p>
            )}
          </Col>
        </Row>
        <Row className="justify-content-md-end">
          <Col xs lg="4">
            {listening && <div className={'dictaphone'} /> }
            <IconButton
              icon={faMicrophone}
              className={'dictaphone'}
              iconSize={"2x"}
              variant={listening ? "danger" : "light"}
              style={{
                width: 100,
                height: 100,
                borderRadius: 100,
                position: "absolute",
                right: 0,
              }}
              onClick={handleChangeListening}
            />
          </Col>
        </Row>
      </Container>
  );
};

Dictaphone.propTypes = propTypes;

const options = {
  autoStart: false,
}

export default SpeechRecognition(options)(Dictaphone);
