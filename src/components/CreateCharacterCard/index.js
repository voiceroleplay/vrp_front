import React from "react";
import { Card } from "react-bootstrap";
import sack from "../../assets/pouch.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export default () => (
  <Link to={"/characters/create"}>
    <Card
      style={{
        borderRadius: 10,
        width: "330px",
        height: "330px",
        padding: 0,
        cursor: "pointer",
        marginBottom: "2rem",
        marginTop: "1rem",
      }}
      className="shadow card-hover"
    >
      <img
        alt={'Sack'}
        className="card-img"
        src={sack}
        style={{
          width: "200px",
          marginTop: "50px",
          height: "auto",
          opacity: "10%",
        }}
      ></img>
      <Card.Body
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          marginTop: "-140px",
        }}
      >
        <FontAwesomeIcon
          size="2x"
          icon={faPlusCircle}
          style={{ color: "#4B3B47" }}
          onClick={() => alert("Add character")}
        />
        <p
          style={{
            fontFamily: "Special Elite",
            color: "#4B3B47",
            fontSize: 18,
            margin: 10,
          }}
        >
          Ajouter un <br></br>personnage
        </p>
      </Card.Body>
    </Card>
  </Link>
);
