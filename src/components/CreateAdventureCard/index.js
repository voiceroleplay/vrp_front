import React from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";

export default () => (
  <Link to={"/adventures/create"}>
    <Card
      style={{
        width: "18rem",
        height: "18rem",
        borderRadius: 10,
        margin: 10,
        padding: 0,
        cursor: "pointer"
      }}
      className="shadow mb-5 card-hover"
    >
      <Card.Body
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <FontAwesomeIcon
          size="2x"
          icon={faPlusCircle}
          style={{ color: "#4B3B47" }}
        />
        <p
          style={{
            fontFamily: "Special Elite",
            color: "#4B3B47",
            fontSize: 18,
            margin: 10,
          }}
        >
          Créer ou rejoindre une aventure
        </p>
      </Card.Body>
    </Card>
  </Link>
);
