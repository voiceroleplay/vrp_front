import React from 'react'
import { Row, Col, Card, Form } from 'react-bootstrap'
import { Image } from 'react-bootstrap'
import './styles.css'

const NO_PICTURE = require('../../assets/no-character.png')

export default ({image, name, race, classe, characteristics, setName}) => (
  <Row className="justify-content-md-center" style={{margin: 10}}>
    <Card className={'character-card shadow'}>
      <Row>
        <Col md={'auto'}>
          <Image className={'character-picture'} src={race.avatar ? race.avatar : NO_PICTURE} />
        </Col>
        <Col>
          <Card className={'character-col'}>
            <Card.Body className={'character-col-body'}>
              <Card.Title className={'character-label'}>NOM</Card.Title>
              <Form.Control className={'character-input'} style={{border: 0, fontSize: 10}} type="text" onChange={(text) => setName('name', text)} />
              <Card.Title className={'character-label'}>RACE</Card.Title>
              <Card.Text className={'character-input'}>{race.name ? race.name : '______'}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col>
          <Card className={'character-col'}>
            <Card.Body className={'character-col-body'}>
              <Card.Title className={'character-label'}>CLASSE</Card.Title>
              <Card.Text className={'character-input'}>{classe.name ? classe.name : '______'}</Card.Text>
              <Card.Title className={'character-label'}>NIVEAU</Card.Title>
              <Card.Text className={'character-input'}>{1}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Card>
  </Row>
  
)