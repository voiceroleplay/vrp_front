import React, { Component } from "react";
import { Router } from "react-router-dom";
import { connect } from "react-redux";
import * as Sentry from "@sentry/browser";

import history from "./services/history";
import Routes from "./routes";
import { alertActions } from "./redux/actions/alert.actions";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Alert } from "react-bootstrap";
import EasterEgg from "./components/EasterEgg"

class App extends Component {
  constructor(props) {
    super(props);
    Sentry.init({
      dsn: "https://890c6f772c2345679685303b4d5998b2@sentry.io/5176653"
    });
    history.listen((location, action) => {
      // clear alert on location change
      this.props.clearAlerts();
    });
    this.state = {
      easterEggVisible: false,
      ctrlKey: false
    }
  }

  handleChangeEasterEgg = (state) => {
    const { easterEggVisible } = this.state
    this.setState({
      easterEggVisible: state ? state : !easterEggVisible
    })
  }

  easterEggMouse = (x, y) => {
    const { ctrlKey } = this.state
    if(ctrlKey && (x < 150) && (y < 150)){
      console.log('easterEggMouse')
      this.handleChangeEasterEgg(true)
    }
  }

  easterEggKeyboard(key){
    if(key === "Control"){
      console.log('Control key pressed')
      this.setState({
        ctrlKey: true
      })
    }
  }

  render() {
    const { alert } = this.props
    const { easterEggVisible } = this.state
    return (
      <Router history={history}>
        <div
          className="App"
          tabIndex="0"
          onMouseMove={(e) => this.easterEggMouse(e.screenX, e.screenY)}
          onKeyDown={(e) => this.easterEggKeyboard(e.key)}
          onKeyUp={() => this.setState({ctrlKey: false})}
        >
          <header className="App-header">
            {alert.message && (
              <Alert variant={alert.type}>{alert.message}</Alert>
            )}
            {easterEggVisible && (
              <EasterEgg show={easterEggVisible} handleClose={this.handleChangeEasterEgg} />
            )}
            <Routes />
          </header>
        </div>
      </Router>
    );
  }
}

const mapState = state => {
  const { alert } = state;
  return { alert };
};

const actionCreators = {
  clearAlerts: alertActions.clear
};

export default connect(mapState, actionCreators)(App);
