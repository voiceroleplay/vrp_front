import { GET_PLAYERS_SUCCESS, GET_PLAYERS_FAILURE, GET_PLAYERS_REQUEST } from "../constants/player.constants";

const initialState = {
    pending: false,
    players: [],
    error: null
}

export const players = (state = initialState, action) => {
  switch (action.type) {
        case GET_PLAYERS_REQUEST:
            return {
                ...state,
                pending: true
            }
        case GET_PLAYERS_SUCCESS:
            return {
                ...state,
                pending: false,
                players: action.players
            }
        case GET_PLAYERS_FAILURE:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state
  }
}