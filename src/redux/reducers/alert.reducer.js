import { SUCCESS, ERROR, CLEAR } from '../constants/alert.constants';

export const alert = (state = {}, action) => {
  switch (action.type) {
    case SUCCESS:
      return {
        type: 'success',
        message: action.message
      };
    case ERROR:
      return {
        type: 'danger',
        message: action.message
      };
    case CLEAR:
      return {};
    default:
      return state
  }
}