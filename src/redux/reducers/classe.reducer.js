import { GET_CLASSES_REQUEST, GET_CLASSES_SUCCESS, GET_CLASSES_FAILURE } from "../constants/classe.constants"

const initialState = {
    pending: false,
    classes: [],
    error: null
}

export const classes = (state = initialState, action) => {
  switch (action.type) {
    case GET_CLASSES_REQUEST:
        return {
            ...state,
            pending: true
        }
    case GET_CLASSES_SUCCESS:
      return {
        ...state,
        pending: false,
        classes: action.classes
      }
    case GET_CLASSES_FAILURE:
        return {
            ...state,
            pending: false,
            error: action.error
        }
    default:
      return state
  }
}