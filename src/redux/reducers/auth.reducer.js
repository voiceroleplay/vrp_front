import {
    LOGIN_FAILURE,
    LOGOUT,
    LOGIN_SUCCESS,
    LOGIN_REQUEST,
    REGISTER_REQUEST,
    REGISTER_SUCCESS,
    REGISTER_FAILURE
} from '../constants/user.constants'

let user = JSON.parse(localStorage.getItem('user'))
const initialState = user ? { loggedIn: true, user } : {}

export const authentication = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_REQUEST:
        return {
            loggingIn: true,
            user: action.user
        };
        case LOGIN_SUCCESS:
        return {
            loggedIn: true,
            user: action.user
        };
        case LOGIN_FAILURE:
        return {}
        case LOGOUT:
        return {}
        default:
        return state
    }
}

export const registration = (state = {}, action) => {
    switch (action.type) {
      case REGISTER_REQUEST:
        return { registering: true };
      case REGISTER_SUCCESS:
        return {};
      case REGISTER_FAILURE:
        return {};
      default:
        return state
    }
}