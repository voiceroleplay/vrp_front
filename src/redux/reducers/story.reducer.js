import { GET_STORIES_SUCCESS, GET_STORIES_FAILURE, GET_STORIES_REQUEST } from "../constants/story.constants";

const initialState = {
    pending: false,
    stories: [],
    error: null
}

export const stories = (state = initialState, action) => {
  switch (action.type) {
    case GET_STORIES_REQUEST:
        return {
            ...state,
            pending: true
        }
    case GET_STORIES_SUCCESS:
      return {
        ...state,
        pending: false,
        stories: action.stories
      }
    case GET_STORIES_FAILURE:
        return {
            ...state,
            pending: false,
            error: action.error
        }
    default:
      return state
  }
}