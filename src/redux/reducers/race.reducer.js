import { GET_RACES_SUCCESS, GET_RACES_FAILURE, GET_RACES_REQUEST } from "../constants/race.constants";

const initialState = {
    pending: false,
    races: [],
    error: null
}

export const races = (state = initialState, action) => {
  switch (action.type) {
    case GET_RACES_REQUEST:
        return {
            ...state,
            pending: true
        }
    case GET_RACES_SUCCESS:
      return {
        ...state,
        pending: false,
        races: action.races
      }
    case GET_RACES_FAILURE:
        return {
            ...state,
            pending: false,
            error: action.error
        }
    default:
      return state
  }
}