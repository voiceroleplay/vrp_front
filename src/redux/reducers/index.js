import { combineReducers } from 'redux'
import { authentication } from './auth.reducer'
import { alert } from './alert.reducer'
import { adventures } from './adventure.reducer'
import { classes } from './classe.reducer'
import { characters } from './character.reducer'
import { races } from './race.reducer'  
import { players } from './player.reducer'
import { stories } from './story.reducer' 


const rootReducer = combineReducers({
  authentication,
  alert,
  adventures,
  classes,
  characters,
  races,
  players,
  stories
})

export default rootReducer