import { GET_ADVENTURES_SUCCESS, GET_ADVENTURES_FAILURE, GET_ADVENTURES_REQUEST } from "../constants/adventure.constants";

const initialState = {
    pending: false,
    adventures: [],
    error: null
}

export const adventures = (state = initialState, action) => {
  switch (action.type) {
    case GET_ADVENTURES_REQUEST:
        return {
            ...state,
            pending: true
        }
    case GET_ADVENTURES_SUCCESS:
      return {
        ...state,
        pending: false,
        adventures: action.adventures
      }
    case GET_ADVENTURES_FAILURE:
        return {
            ...state,
            pending: false,
            error: action.error
        }
    default:
      return state
  }
}