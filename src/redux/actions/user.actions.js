import {
    LOGIN_REQUEST,
    LOGIN_FAILURE,
    LOGIN_SUCCESS,
    LOGOUT,
    REGISTER_REQUEST,
    REGISTER_SUCCESS,
    REGISTER_FAILURE
} from '../constants/user.constants'
import { alertActions } from './alert.actions'
import history from '../../services/history'
import { userService } from '../../services/user'
import axios from 'axios'
import { API_BASE_URL } from '../../config/api.conf'
import { customRequest } from '../../services/request'

const login = (email, password) => {
  return (dispatch) => {
    dispatch(request({ email }));
    userService
      .login(email, password)
      .then((user) => {
        customRequest(
          `/account?email=${user.user.email}`,
          'get',
        )
        .then((userAccount) => {
          console.log('FOUND USER ACCOUNT ON API', userAccount)
          const newUser = {
            ...userAccount[0],
            ...user
          }
          console.log("USER", newUser);
          dispatch(success(newUser));
          history.push("/");
        })
        .catch((error) => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        });
      })
      .catch((error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };

  function request(user) {
    return { type: LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: LOGIN_FAILURE, error };
  }
};

const loginGoogle = () => {
  return (dispatch) => {
    userService
      .loginGoogle()
      .then((user) => {
        console.log("USER", user);
        dispatch(success(user));
        history.push("/");
      })
      .catch((error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };
  function success(user) {
    return { type: LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: LOGIN_FAILURE, error };
  }
};

const loginFB = () => {
  return (dispatch) => {
    userService
      .loginFB()
      .then((user) => {
        console.log("USER", user);
        dispatch(success(user));
        history.push("/");
      })
      .catch((error) => {
        dispatch(failure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      });
  };
  function success(user) {
    return { type: LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: LOGIN_FAILURE, error };
  }
};

const loginTw = () => {
    return (dispatch) => {
      userService
        .loginTw()
        .then((user) => {
          console.log("USER", user);
          dispatch(success(user));
          history.push("/");
        })
        .catch((error) => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        });
    };
    function success(user) {
      return { type: LOGIN_SUCCESS, user };
    }
    function failure(error) {
      return { type: LOGIN_FAILURE, error };
    }
  };

const logout = () => {
  userService.logout();
  return { type: LOGOUT };
};

const register = (firstname, lastname, email, password, passwordConfirm) => {
    return dispatch => {
        if (password !== passwordConfirm) {
            const error = 'Mot de passe invalide'
            dispatch(failure(error))
            dispatch(alertActions.error(error))
        }
        dispatch(request({ email, password }));
        userService.register(email, password)
            .then((user) => {
                console.log('User account created on Firebase', user)
                const username = generateUsername(firstname, lastname)
                customRequest(
                    '/account',
                    'post',
                    null,
                    {
                        lastname,
                        firstname,
                        username,
                        email
                    }
                )
                .then((user) => {
                    console.log('USER', user)
                    dispatch(success())
                    history.push('/login')
                    dispatch(alertActions.success('Registration successful'))
                })
                .catch((error) => {
                    dispatch(failure(error.toString()))
                    dispatch(alertActions.error(error.toString()))
                })
            })
            .catch((error) => {
                dispatch(failure(error.toString()))
                dispatch(alertActions.error(error.toString()))
            })
    };

  function request(user) {
    return { type: REGISTER_REQUEST, user };
  }
  function success(user) {
    return { type: REGISTER_SUCCESS, user };
  }
  function failure(error) {
    return { type: REGISTER_FAILURE, error };
  }
};

const generateUsername = (firstname, lastname) => {
    const firstnameFormatted = firstname
        .replace(/[éè]/g, 'e')
        .replace(/[à]/g, 'a')
        .replace(/[^\w\*]/g, '')
        .toLowerCase()
        .slice(0,4)
    const lastnameFormatted = lastname
        .replace(/[éè]/g, 'e')
        .replace(/[à]/g, 'a')
        .replace(/[^\w\*]/g, '')
        .toLowerCase()
        .slice(0,10)
    const username = `${firstnameFormatted}.${lastnameFormatted}`
    return username
}

export const userActions = {
  login,
  loginGoogle,
  loginFB,
  loginTw,
  logout,
  register,
};
