import { alertActions } from './alert.actions'
import history from '../../services/history'
import { GET_RACES_SUCCESS, GET_RACES_FAILURE, GET_RACES_REQUEST } from '../constants/race.constants';
import { customRequest } from '../../services/request';

export const getRaces = (username) => {
    return dispatch => {
        dispatch(request(username));
        customRequest(
            '/race',
            'get',
            { username }
        )
        .then((races) => {
            console.log("RACES", races)
            dispatch(success(races));
        })
        .catch((error) => {
            dispatch(failure(error.toString()));
            dispatch(alertActions.error(error.toString()));
        });
    };

    function request(username) { return { type: GET_RACES_REQUEST, username } }
    function success(races) { return { type: GET_RACES_SUCCESS, races } }
    function failure(error) { return { type: GET_RACES_FAILURE, error } }
}
