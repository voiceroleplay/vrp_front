import { alertActions } from './alert.actions'
import { GET_STORIES_SUCCESS, GET_STORIES_FAILURE, GET_STORIES_REQUEST } from '../constants/story.constants';
import { customRequest } from '../../services/request';

export const getStories = () => {
    return dispatch => {
        dispatch(request());
        customRequest(
            '/story',
            'get',
        )
        .then((stories) => {
            console.log("STORIES", stories)
            dispatch(success(stories));
        })
        .catch((error) => {
            dispatch(failure(error.toString()));
            dispatch(alertActions.error(error.toString()));
        });
    };

    function request() { return { type: GET_STORIES_REQUEST } }
    function success(stories) { return { type: GET_STORIES_SUCCESS, stories } }
    function failure(error) { return { type: GET_STORIES_FAILURE, error } }
}
