import { alertActions } from './alert.actions'
import history from '../../services/history'
import { customRequest } from '../../services/request';
import { GET_CHARACTERS_REQUEST, GET_CHARACTERS_FAILURE, GET_CHARACTERS_SUCCESS } from '../constants/character.constants';

export const getCharacters = (username) => {
    return dispatch => {
        dispatch(request(username));
        customRequest(
            `/personnage?username=${username}`,
            'get',
            { username }
        )
        .then((characters) => {
            console.log("CHARACTERS", characters)
            dispatch(success(characters));
        })
        .catch((error) => {
            dispatch(failure(error.toString()));
            dispatch(alertActions.error(error.toString()));
        });
    };

    function request(username) { return { type: GET_CHARACTERS_REQUEST, username } }
    function success(characters) { return { type: GET_CHARACTERS_SUCCESS, characters } }
    function failure(error) { return { type: GET_CHARACTERS_FAILURE, error } }
}
