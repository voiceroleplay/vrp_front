import { alertActions } from './alert.actions'
import { GET_PLAYERS_REQUEST, GET_PLAYERS_SUCCESS, GET_PLAYERS_FAILURE } from "../constants/player.constants"
import { customRequest } from '../../services/request';

export const getPlayers = () => {
    return dispatch => {
        dispatch(request());
        customRequest(
            '/account',
            'get',
        )
        .then((players) => {
            console.log("PLAYERS", players)
            dispatch(success(players));
        })
        .catch((error) => {
            dispatch(failure(error.toString()));
            dispatch(alertActions.error(error.toString()));
        });
    };

    function request() { return { type: GET_PLAYERS_REQUEST } }
    function success(players) { return { type: GET_PLAYERS_SUCCESS, players } }
    function failure(error) { return { type: GET_PLAYERS_FAILURE, error } }
}
