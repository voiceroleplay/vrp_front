import { alertActions } from './alert.actions'
import history from '../../services/history'
import { GET_ADVENTURES_SUCCESS, GET_ADVENTURES_FAILURE, GET_ADVENTURES_REQUEST } from '../constants/adventure.constants';
import { customRequest } from '../../services/request';

export const getAdventures = (email) => {
    return dispatch => {
        dispatch(request(email));
        customRequest(
            '/aventure?email='+email,
            'get',
        )
        .then((adventures) => {
            console.log("ADVENTURES", adventures)
            dispatch(success(adventures));
        })
        .catch((error) => {
            dispatch(failure(error.toString()));
            dispatch(alertActions.error(error.toString()));
        });
    };

    function request(email) { return { type: GET_ADVENTURES_REQUEST, email } }
    function success(adventures) { return { type: GET_ADVENTURES_SUCCESS, adventures } }
    function failure(error) { return { type: GET_ADVENTURES_FAILURE, error } }
}
