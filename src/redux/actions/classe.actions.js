import { alertActions } from './alert.actions'
import history from '../../services/history'
import { customRequest } from '../../services/request';
import { GET_CLASSES_REQUEST, GET_CLASSES_SUCCESS, GET_CLASSES_FAILURE } from '../constants/classe.constants';

export const getClasses = () => {
    return dispatch => {
        dispatch(request());
        customRequest(
            '/classe',
            'get'
        )
        .then((classes) => {
            console.log("CLASSES", classes)
            dispatch(success(classes));
        })
        .catch((error) => {
            dispatch(failure(error.toString()));
            dispatch(alertActions.error(error.toString()));
        });
    };

    function request() { return { type: GET_CLASSES_REQUEST } }
    function success(classes) { return { type: GET_CLASSES_SUCCESS, classes } }
    function failure(error) { return { type: GET_CLASSES_FAILURE, error } }
}
