import app from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g",
  authDomain: "voiceroleplay-b3ae9.firebaseapp.com",
  databaseURL: "https://voiceroleplay-b3ae9.firebaseio.com",
  projectId: "voiceroleplay-b3ae9",
  storageBucket: "voiceroleplay-b3ae9.appspot.com",
  messagingSenderId: "655583604240",
  appId: "1:655583604240:web:ab846e50b945f81a89e495",
  measurementId: "G-TS9BCSW7RL",
};

app.initializeApp(firebaseConfig);

const login = (email, password) => {
  return app
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((user) => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem("user", JSON.stringify(user));
      return user;
    })
    .catch((err) => console.error(err));
};

const loginGoogle = () => {
  var provider = new app.auth.GoogleAuthProvider();

  return app
    .auth()
    .signInWithPopup(provider)
    .then((user) => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem("user", JSON.stringify(user));
      return user;
    })
    .catch((err) => console.error(err));
};

const loginFB = () => {
  var provider = new app.auth.FacebookAuthProvider();

  return app
    .auth()
    .signInWithPopup(provider)
    .then((user) => {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem("user", JSON.stringify(user));
      return user;
    })
    .catch((err) => console.error(err));
};

const loginTw = () => {
    var provider = new app.auth.TwitterAuthProvider();
  
    return app
      .auth()
      .signInWithPopup(provider)
      .then((user) => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem("user", JSON.stringify(user));
        return user;
      })
      .catch((err) => console.error(err));
  };

const logout = () => {
  localStorage.removeItem("user");
  localStorage.removeItem('@authToken')
  return (
    app
      .auth()
      .signOut()
      .then(() => window.location.reload(true))
      .catch((err) => console.error(err))
  );
};

const register = (email, password) => {
  return app.auth().createUserWithEmailAndPassword(email, password);
};

export const userService = {
  login,
  loginGoogle,
  loginFB,
  loginTw,
  logout,
  register,
};
