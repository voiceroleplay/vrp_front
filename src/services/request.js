import axios from "axios"
import { API_BASE_URL } from "../config/api.conf"

export const generateAuthToken = async () => {
    const { data } = await axios.get(`${API_BASE_URL}/authToken`)
    const authToken = data.token
    const expirationDate = new Date().getTime() + 800000
    const dataToLocalStore = { authToken, expirationDate }
    localStorage.setItem('@authToken', JSON.stringify(dataToLocalStore))
    return authToken
}

const getAuthToken = async () => {
    const token = localStorage.getItem('@authToken')
    if(token === null) {
        const newToken = await generateAuthToken()
        return newToken
    } else {
        const { authToken, expirationDate } = JSON.parse(token)
        if (expirationDate < new Date().getTime()){
            const newToken = await generateAuthToken()
            return newToken
        } else {
            console.log('USE SAME TOKEN', authToken)
            return authToken
        }
    }
}

export const customRequest = async (url, method, headersMore, data) => {
    const authToken = await getAuthToken()
    const initialHeader = { 
        'authtoken': authToken,
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
    }
    const headers = { headers: {...initialHeader, ...headersMore} }
    switch (method) {
        case "get":
            return axios.get(`${API_BASE_URL}${url}`, headers)
                .then((res) => res.data)
                .catch((err) => {
                    console.error(err)
                })
        case "post":
            return axios.post(`${API_BASE_URL}${url}`, data, headers)
                .then((res) => res.data)
                .catch((err) => {
                    console.error(err)
                })
        case "put":
            return axios.put(`${API_BASE_URL}${url}`, data, headers)
                .then((res) => res.data)
                .catch((err) => {
                    console.error(err)
                })
        case "delete":
            return axios.delete(`${API_BASE_URL}${url}`, data, headers)
                .then((res) => res.data)
                .catch((err) => {
                    console.error(err)
                })
        default:
            break;
    }
}
