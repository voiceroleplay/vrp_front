import React, { useState } from "react";
import { Form, Row, Col, Card, Button, Accordion } from "react-bootstrap";
import NavbarCustom from "../../components/NavbarCustom/";
import { userActions } from "../../redux/actions/user.actions";
import { getAdventures } from "../../redux/actions/adventure.actions";

import { connect } from "react-redux";

import IconButton from "../../components/IconButton";
import adventuresData from "../../mocks/adventures.json";
import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { getPlayers } from "../../redux/actions/player.actions";

const CreateStory = ({ match, logout, user }) => {
  const { adventures } = adventuresData;

  const [isSpinnerShow, setIsSpinnerShow] = useState(false);
  const signOut = () => {
    setIsSpinnerShow(true);
    logout();
  };

  const [phaseList, setPhaseList] = useState([]);
  const [, setCurrentPhase] = useState({
    monstre: "",
    nbMonstre: "",
  });

  const handleChangeMonstre = (e, index) => {
    const phases = [...phaseList];
    phases[index].monstre = e.target.value;
  };

  const handleChangeNombre = (e, index) => {
    const phases = [...phaseList];
    phases[index].nbMonstre = e.target.value;
  };

  const addPhase = () => {
    const phases = [...phaseList];
    phases.push({ monstre: "", nbMonstre: "" });
    setPhaseList(phases);
  };

  const removePhase = (index) => {
    const phases = [...phaseList];
    phases.splice(index, 1);
    setPhaseList(phases);
  };

  return (
    <div>
      <NavbarCustom />
      <h2 style={{ color: "#4B3B47" }}>Créer une histoire</h2>
      <Card
        style={{
          width: "80%",
          padding: 20,
          display: "block",
          margin: "0 auto",
          marginTop: "1rem",
          marginBottom: "1rem",
        }}
        className="shadow"
      >
        <Row>
          <Col>
            <Form.Group>
              <Form.Label
                style={{
                  color: "#8a8a8a",
                  display: "block",
                  textAlign: "left",
                }}
              >
                Titre
              </Form.Label>
              <Form.Control type="text" placeholder="Titre de l'aventure" />
            </Form.Group>
            <Form.Group>
              <Form.Label
                style={{
                  color: "#8a8a8a",
                  display: "block",
                  textAlign: "left",
                }}
              >
                Photo
              </Form.Label>
              <IconButton
                icon={faCamera}
                iconSize={"2x"}
                onClick={() => alert("Upload picture")}
                style={{
                  width: "30vh",
                  height: "30vh",
                  display: "block",
                  margin: "0 auto",
                }}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label
                style={{
                  color: "#8a8a8a",
                  display: "block",
                  textAlign: "left",
                }}
              >
                Résumé
              </Form.Label>
              <Form.Control as="textarea" placeholder="Il était une fois..." />
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col>
            <Form.Group>
              <Form.Label
                style={{
                  color: "#8a8a8a",
                  display: "block",
                  textAlign: "left",
                }}
              >
                Phase
              </Form.Label>

              <Button onClick={() => addPhase()} style={{ width: "80%" }}>
                Ajouter phase
              </Button>

              {phaseList.map((phase, index) => {
                return (
                  <Accordion
                    defaultActiveKey="0"
                    style={{
                      color: "#4B3B47",
                      width: "80%",
                      margin: "0.5rem auto 0 auto",
                      color: "#4B3B47",
                      fontWeight: "bold",
                      cursor: "pointer",
                    }}
                  >
                    <Card>
                      <Accordion.Toggle as={Card.Header} eventKey="1">
                        Phase {index + 1}
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="1">
                        <Card.Body>
                          <Form.Group>
                            <Form.Label
                              style={{
                                color: "#8a8a8a",
                                display: "block",
                                textAlign: "left",
                              }}
                            >
                              Monstre
                            </Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Loups"
                              onChange={(e) => handleChangeMonstre(e, index)}
                            />
                            <Form.Label
                              style={{
                                color: "#8a8a8a",
                                display: "block",
                                textAlign: "left",
                              }}
                            >
                              Nombre de monstres
                            </Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="2"
                              onChange={(e) => handleChangeNombre(e, index)}
                            ></Form.Control>
                            <Button
                              onClick={() => removePhase(index)}
                              style={{ marginTop: "1rem" }}
                            >
                              Supprimer phase
                            </Button>
                          </Form.Group>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                );
              })}
            </Form.Group>
          </Col>
        </Row>
      </Card>
      <Button>Créer histoire</Button>
    </div>
  );
};

const mapState = (state) => {
  const { loggingIn, user } = state.authentication;
  const { adventures } = state.adventures;
  const { players } = state.players;
  return { loggingIn, user, adventures, players };
};

const mapDispatchToProps = {
  logout: userActions.logout,
  getAdventures: getAdventures,
  getPlayers: getPlayers,
};

export default connect(mapState, mapDispatchToProps)(CreateStory);
