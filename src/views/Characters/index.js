import React from "react";
import { Container } from "react-bootstrap";
import { connect } from "react-redux";

import { getCharacters } from "../../redux/actions/character.actions"

import NavbarCustom from "../../components/NavbarCustom";
import CharacterCard from "../../components/CharacterCard";
import CreateCharacterCard from "../../components/CreateCharacterCard";
import EmptyCharacterCard from "../../components/EmptyCharacterCard";
import "./index.css";

class Characters extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      setIsSpinnerShow: false
    } 
  }

  componentWillMount(){
    const { username } = this.props.user
    this.props.getCharacters(username)
  }

  setIsSpinnerShow = (bool) => {
    this.setState({
      isSpinnerShow: bool
    })
  }

  render(){
    const { characters } = this.props

    const nbEmptyCharac = 6 - (characters.length + 1);
    const emptyCharac = new Array(nbEmptyCharac).fill("");
    return(
      <Container>
        <NavbarCustom />
        <div className="card-columns">
          {characters.map((personnage, index) => (
            <CharacterCard key={index} personnage={personnage} />
          ))}
          {characters.length < 6 ? (
            <CreateCharacterCard idNewCharac={characters.length + 1} />
          ) : null}
          {emptyCharac.map((elem, index) => (
            <EmptyCharacterCard
              key={index}
              personnage={characters}
              index={index}
            />
          ))}
        </div>
      </Container>
    )
  }
} 

const mapState = state => {
  const { user } = state.authentication;
  const { characters } = state.characters
  return { characters, user };
};

const mapDispatchToProps = {
  getCharacters: getCharacters
};

export default connect(mapState, mapDispatchToProps)(Characters);
