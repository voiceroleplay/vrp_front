import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Button, Container, Row, Col, Modal } from "react-bootstrap";
import { StyledSpinner, SpinnerContainer } from "./useStyle.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { userActions } from "../../redux/actions/user.actions";
import { connect } from "react-redux";

import {
  faFacebookSquare,
  faTwitterSquare,
  faGithubSquare,
  faGooglePlusSquare,
} from "@fortawesome/free-brands-svg-icons";
import Logo from "./../../assets/logo-voiceroleplay-light.svg";
import { Link } from "react-router-dom";

const Login = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isSpinnerShow, setIsSpinnerShow] = useState(false);

  const signIn = () => {
    setIsSpinnerShow(true);
    props.login(email, password);
  };

  const signInGoogle = () => {
    setIsSpinnerShow(true);
    props.loginGoogle();
  };

  const signInFB = () => {
    setIsSpinnerShow(true);
    console.log(props);
    props.loginFB();
  };

  const signInTw = () => {
    setIsSpinnerShow(true);
    props.loginTw();
  };

  const handleChange = (event) => {
    if (event.target.name === "email") {
      setEmail(event.target.value);
    }
    if (event.target.name === "password") {
      setPassword(event.target.value);
    }
  };

  return (
    <Container>
      <Row className="justify-content-md-center">
        <Col md={4} lg={4}>
          <Modal.Dialog>
            <Modal.Body>
              <img src={Logo} className="App-logo" alt="logo" />
              <Form>
                <Form.Group controlId="formBasicEmail">
                  <Form.Control
                    name="email"
                    type="email"
                    placeholder="Adresse e-mail"
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
                {isSpinnerShow ? (
                  <SpinnerContainer>
                    <StyledSpinner animation="border" variant="dark" size="l" />
                  </SpinnerContainer>
                ) : null}
                <Form.Group
                  controlId="formBasicPassword"
                  style={{ marginBottom: 40 }}
                >
                  <Form.Control
                    name="password"
                    type="password"
                    placeholder="Mot de passe"
                    onChange={(e) => handleChange(e)}
                  />
                </Form.Group>
                <Button
                  variant="primary"
                  style={{ marginBottom: 10 }}
                  onClick={() => signIn()}
                >
                  Se connecter
                </Button>
              </Form>
              <div
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <a
                  href="#facebook"
                  className={"social-login"}
                  style={{ textDecoration: "none", margin: 5 }}
                >
                  <FontAwesomeIcon
                    icon={faFacebookSquare}
                    onClick={() => signInFB()}
                  />
                </a>
                <a
                  href="#twitter"
                  className={"social-login"}
                  style={{ textDecoration: "none", margin: 5 }}
                >
                  <FontAwesomeIcon
                    icon={faTwitterSquare}
                    onClick={() => signInTw()}
                  />
                </a>
                <a
                  href="#google"
                  className={"social-login"}
                  style={{ textDecoration: "none", margin: 5 }}
                >
                  <FontAwesomeIcon
                    icon={faGooglePlusSquare}
                    onClick={() => signInGoogle()}
                  />
                </a>
              </div>
              <Link data-testid="register-link" to="/register">
                <p>S'inscrire</p>
              </Link>
            </Modal.Body>
          </Modal.Dialog>
        </Col>
      </Row>
    </Container>
  );
};

const mapState = (state) => {
  const { loggingIn } = state.authentication;
  return { loggingIn };
};

const mapDispatchToProps = {
  login: userActions.login,
  loginGoogle: userActions.loginGoogle,
  loginFB: userActions.loginFB,
  loginTw: userActions.loginTw,
};

export default connect(mapState, mapDispatchToProps)(Login);
