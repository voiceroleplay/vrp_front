import { Spinner } from "react-bootstrap";
import styled from "styled-components";

export const SpinnerContainer = styled.div`
  opacity: 0.8;
  background-color: #ccc;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  z-index: 1000;
  opacity: 60%;
`;

export const StyledSpinner = styled(Spinner)`
  position: relative;
  top: 50%;
`;

export default StyledSpinner;
