import React from "react";
import { Container, Card } from "react-bootstrap";
import NavbarCustom from "../../components/NavbarCustom";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { user } = this.props.user
    return (
      <Container>
        <NavbarCustom />
        <Card
          style={{
            width: "80%",
            padding: 20,
            display: "block",
            margin: "0 auto",
            marginTop: "1rem",
            marginBottom: "1rem",
          }}
          className="shadow"
        >
          <div style={{ display: "inline-block", marginRight: 10 }}>
            {user.photoUrl ? (
              <img alt="" src={user.photoUrl} width="40" height="40" />
            ) : (
              <FontAwesomeIcon size={"4x"} icon={faUserCircle} style={{ color: "#000" }} />
            )}
          </div>
          <h2 style={{color: "#4B3B47"}}>Profile</h2>
        </Card>
      </Container>
    );
  }
}

const mapState = (state) => {
  const { user } = state.authentication;
  return { user };
};

export default connect(mapState)(Profile);
