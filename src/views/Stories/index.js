import React, { useState } from "react";
import {
  Accordion,
  Card,
  Button,
} from "react-bootstrap";
import NavbarCustom from "../../components/NavbarCustom/";
import { connect } from "react-redux";

import { useMediaQuery } from "react-responsive";
import { Link } from "react-router-dom";
import { getStories } from "../../redux/actions/story.action";

class Stories extends React.Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentWillMount(){
    this.props.getStories()
  }

  render(){
    const { stories } = this.props
    return (
      <div>
        <NavbarCustom />
        <Link to={"/story/create"}>
          <Button>Nouvelle histoire</Button>
        </Link>
        {stories.map((story) => {
          return (
            <Accordion
              defaultActiveKey="0"
              style={{
                color: "#4B3B47",
                width: "80%",
                margin: "0.5rem auto 0 auto",
                fontWeight: "bold",
                cursor: "pointer",
              }}
            >
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                  {story.name}
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                  <Card.Body>{story.text}</Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          );
        })}
      </div>
    )
  }
}

const mapState = (state) => {
  const { stories } = state.stories;
  return { stories };
};

const mapDispatchToProps = {
  getStories: getStories,
};

export default connect(mapState, mapDispatchToProps)(Stories);
