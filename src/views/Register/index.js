import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Button, Container, Row, Col, Modal } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookSquare,
  faTwitterSquare,
  faGithubSquare,
  faGooglePlusSquare
} from "@fortawesome/free-brands-svg-icons";

import { connect } from "react-redux";
import { userActions } from "../../redux/actions/user.actions";

import Logo from "./../../assets/logo-voiceroleplay-light.svg";
import StyledSpinner, { SpinnerContainer } from "../Login/useStyle";
import { Link } from "react-router-dom";

const Register = props => {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [isSpinnerShow, setIsSpinnerShow] = useState(false);

  const signUp = () => {
    setIsSpinnerShow(true);
    props.register(firstname, lastname, email, password, passwordConfirm)
  }

  const handleChange = event => {
    const { name, value } = event.target;
    switch (name) {
      case "firstname":
        setFirstname(value);
        break;
      case "lastname":
        setLastname(value);
        break;
      case "email":
        setEmail(value);
        break;
      case "password":
        setPassword(value);
        break;
      case "passwordConfirm":
        setPasswordConfirm(value);
        break;

      default:
        break;
    }
  };
  return (
    <Container>
      <Row className="justify-content-md-center">
        <Col md={4} lg={4}>
          <Modal.Dialog>
            <Modal.Body>
              <img src={Logo} className="App-logo" alt="logo" />
              <Form>
                <Form.Group controlId="formBasicFirstname">
                  <Form.Control
                    name="firstname"
                    type="firstname"
                    placeholder="Prénom"
                    onChange={e => handleChange(e)}
                  />
                </Form.Group>
                <Form.Group controlId="formBasicLastname">
                  <Form.Control
                    name="lastname"
                    type="lastname"
                    placeholder="Nom"
                    onChange={e => handleChange(e)}
                  />
                </Form.Group>
                <Form.Group controlId="formBasicEmail">
                  <Form.Control
                    name="email"
                    type="email"
                    placeholder="Adresse e-mail"
                    onChange={e => handleChange(e)}
                  />
                </Form.Group>
                {isSpinnerShow ? (
                  <SpinnerContainer>
                    <StyledSpinner animation="border" variant="dark" size="l" />
                  </SpinnerContainer>
                ) : null}
                <Form.Group
                  controlId="formBasicPassword"
                >
                  <Form.Control
                    name="password"
                    type="password"
                    placeholder="Mot de passe"
                    onChange={e => handleChange(e)}
                  />
                </Form.Group>
                <Form.Group
                  controlId="formBasicPasswordConfirm"
                  style={{ marginBottom: 40 }}
                >
                  <Form.Control
                    name="passwordConfirm"
                    type="password"
                    placeholder="Confirmer mot de passe"
                    onChange={e => handleChange(e)}
                  />
                </Form.Group>
                <Button
                  variant="primary"
                  style={{ marginBottom: 10 }}
                  onClick={() => signUp()}
                >
                  Créer un compte
                </Button>
              </Form>
              <div
                style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <a
                  href="#facebook"
                  className={"social-login"}
                  style={{ textDecoration: "none", margin: 5 }}
                >
                  <FontAwesomeIcon icon={faFacebookSquare} />
                </a>
                <a
                  href="#twitter"
                  className={"social-login"}
                  style={{ textDecoration: "none", margin: 5 }}
                >
                  <FontAwesomeIcon icon={faTwitterSquare} />
                </a>
                <a
                  href="#google"
                  className={"social-login"}
                  style={{ textDecoration: "none", margin: 5 }}
                >
                  <FontAwesomeIcon icon={faGooglePlusSquare} />
                </a>
                <a
                  href="#github"
                  className={"social-login"}
                  style={{ textDecoration: "none", margin: 5 }}
                >
                  <FontAwesomeIcon icon={faGithubSquare} />
                </a>
              </div>
              <Link data-testid="login-link" to="/login">
                <p>Se connecter</p>
              </Link>
            </Modal.Body>
          </Modal.Dialog>
        </Col>
      </Row>
    </Container>
  );
};

const mapState = state => {
  const { loggingIn } = state.authentication;
  return { loggingIn };
};

const mapDispatchToProps = {
  register: userActions.register
} 

export default connect(mapState, mapDispatchToProps)(Register);