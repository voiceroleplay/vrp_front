import React from "react";
import { Form, Row, Col, Card, Button, Container } from "react-bootstrap";
import { connect } from "react-redux";

import { getPlayers } from "../../redux/actions/player.actions";

import NavbarCustom from "../../components/NavbarCustom";
import IconButton from "../../components/IconButton"
import SelectionAutocomplete from "../../components/SelectionAutocomplete";

import { faCamera } from "@fortawesome/free-solid-svg-icons";

class CreateAdventure extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isSpinnerShow: false,
      name: "",
      picture: "",
      playersSelected: [],
      storySelected: {}
    }
  }

  componentWillMount(){
    this.props.getPlayers()
  }

  setIsSpinnerShow = (bool) => {
    this.setState({
      isSpinnerShow: bool
    })
  }

  signOut = () => {
    this.setIsSpinnerShow(true);
    this.props.logout();
  }

  renderSelectPlayer = (option) => (
    <div>
      {option.username}
      <div>
        <small>Nom: {`${option.firstname} ${option.lastname}`}</small>
      </div>
    </div>
  )

  renderSelectStory = (option) => (
    <div>
      <small>Nom: {option}</small>
    </div>
  )

  handleChangeText = (e) => {
    const { name, value } = e.target
    this.setState({
      [name]: value
    })
  }

  handleChangeSelect = (name, value) => {
    this.setState({
      [name]: value
    })
  }

  createAdventure = () => {
    const { name, picture, playersSelected, storySelected } = this.state
    const data = {
      name,
      picture,
      playersSelected,
      storySelected
    }
    console.log("CREATE ADVENTURE", data)
  }

  render(){
    const { match, players } = this.props;
    const { name, picture, playersSelected, storySelected } = this.state
    return (
      <Container>
        <NavbarCustom />
        <h2>Créer une aventure</h2>
        <Card
          style={{
            width: "80%",
            padding: 20,
            display: "block",
            margin: "0 auto",
            marginTop: "1rem",
            marginBottom: "1rem",
          }}
          className="shadow"
        >
          <Row>
            <Col>
              <Form.Group>
                <Form.Label style={{
                  color: '#8a8a8a',
                  display: 'block',
                  textAlign: 'left'
                }}>Titre</Form.Label>
                <Form.Control
                  name={"name"}
                  value={name}
                  onChange={this.handleChangeText}
                  type="text"
                  placeholder="Mystérium"
                />
              </Form.Group>
              <Form.Group>
                <Form.Label style={{
                  color: '#8a8a8a',
                  display: 'block',
                  textAlign: 'left'
                }}>Photo</Form.Label>
                <IconButton
                  icon={faCamera}
                  iconSize={'2x'}
                  onClick={() => alert('Upload picture')}
                  style={{
                    width: '30vh',
                    height: '30vh',
                    display: 'block',
                    margin: '0 auto'
                  }}
                />
              </Form.Group>
            </Col>
            <Col>
              <Form.Group>
                <Form.Label style={{
                  color: '#8a8a8a',
                  display: 'block',
                  textAlign: 'left'
                }}>Joueurs</Form.Label>
                <SelectionAutocomplete
                  name={'username'}
                  message={'Ajouter des joueurs'}
                  data={players || []}
                  renderMenuItemChildren={this.renderSelectPlayer}
                  multiple={true}
                  onChange={(players) => this.handleChangeSelect("playersSelected", players)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label style={{
                  color: '#8a8a8a',
                  display: 'block',
                  textAlign: 'left'
                }}>Histoire</Form.Label>
                <SelectionAutocomplete
                  name={'username'}
                  message={'Sélectionner une histoire'}
                  data={[
                    'Mystérium'
                  ] || []}
                  renderMenuItemChildren={this.renderSelectStory}
                  multiple={false}
                  onChange={(story) => this.handleChangeSelect("storySelected", story)}
                />
              </Form.Group>
            </Col>
          </Row>
        </Card>
          <Button
            onClick={this.createAdventure}
          >
            Créer l'aventure
          </Button>
      </Container>
    )
  }
}
const mapState = (state) => {
  const { players } = state.players
  return { players };
};

const mapDispatchToProps = {
  getPlayers: getPlayers,
};

export default connect(mapState, mapDispatchToProps)(CreateAdventure);
