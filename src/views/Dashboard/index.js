import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, CardDeck } from "react-bootstrap";
import { StyledSpinner, SpinnerContainer } from "../Login/useStyle.js";

import NavbarCustom from "../../components/NavbarCustom";
import AdventureCard from "../../components/AdventureCard/";

import { getAdventures } from "../../redux/actions/adventure.actions";

import { connect } from "react-redux";

import CreateAdventureCard from "../../components/CreateAdventureCard/index.js";
import Dictaphone from "../../components/Dictaphone/index.js";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSpinnerShow: false,
    };
  }

  componentDidMount() {
    const { email } = this.props.user.user
    this.props.getAdventures(email);
  }

  signOut = () => {
    this.handleChangeIsSpinnerShow(true);
    this.props.logout();
  };

  handleChangeIsSpinnerShow = (bool) => {
    this.setState({
      isSpinnerShow: bool,
    });
  };

  render() {
    const { user } = this.props.user;
    const { isSpinnerShow } = this.state;
    const { adventures } = this.props;
    return (
      <Container>
        <NavbarCustom />
        <CardDeck>
          {(adventures && adventures.length > 0) &&
              adventures.map((a, index) => (
              <AdventureCard
                key={index}
                adventure={a}
                isAdmin={a.owner === user.uid}
              />
            ))
          }
          <CreateAdventureCard />
        </CardDeck>
        <Dictaphone />
        {isSpinnerShow ? (
          <SpinnerContainer>
            <StyledSpinner animation="border" variant="dark" size="l" />
          </SpinnerContainer>
        ) : null}
      </Container>
    );
  }
}

const mapState = (state) => {
  const { user } = state.authentication;
  const { adventures } = state.adventures;
  return { user, adventures };
};

const mapDispatchToProps = {
  getAdventures: getAdventures,
};

export default connect(mapState, mapDispatchToProps)(Dashboard);
