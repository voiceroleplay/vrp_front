import React from "react";
import { Button, Container } from "react-bootstrap";
import { connect } from "react-redux";

import { customRequest } from "../../services/request";

import { getClasses } from "../../redux/actions/classe.actions";
import { getRaces } from "../../redux/actions/race.actions";

import NavbarCustom from "../../components/NavbarCustom";
import Race from "../../components/Race";
import Classe from "../../components/Classe";
import Caracteristiques from "../../components/Caracteristiques";
import Description from "../../components/Description";
import Equipements from "../../components/Equipements";
import StepBar from "../../components/StepBar";
import StepCharacter from "../../components/StepCharacter";

class CreateCharac extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      isSpinnerShow: false,
      currentStep: 1,
      image: "",
      name: "",
      race: "",
      classe: "",
      characteristics: {
        strength: { id: "strength", value: -1 },
        dexterity: { id: "dexterity", value: -1 },
        constitution: { id: "constitution", value: -1 },
        intelligence: { id: "intelligence", value: -1 },
        wisdom: { id: "wisdom", value: -1 },
        charisma: { id: "charisma", value: -1 }
      }
    }
  }

  componentWillMount() {
    this.props.getClasses()
    this.props.getRaces()
  }

  handleChange = (name, value) => {
    this.setState({
      [name]: value
    })
  }

  signOut = () => {
    this.handleChange("isSpinnerShow", true);
    this.props.logout();
  }

  createPersonnage = () => {
    const data = {
      userName: this.state.name,
      gameName: 'testAdventure',
      sheet: {}
    }
    this.handleChange("isSpinnerShow", true);
    customRequest(
      '/personnage', 
      'post', 
      data
    )
    .then((personnage) => {
      console.log(personnage)
      this.handleChange("isSpinnerShow", false);
    })
    .catch((err) => {
      console.error(err)
      this.handleChange("isSpinnerShow", false);
    })
  }

  render() {
    const { classes, races } = this.props
    const {
      currentStep,
      image,
      name,
      race,
      classe,
      characteristics
    } = this.state

    return (
      <Container>
        <NavbarCustom />
        <StepBar
          steps={[
            { id: 1, name: "1. Race" },
            { id: 2, name: "2. Classe" },
            { id: 3, name: "3. Caracteristiques" },
            { id: 4, name: "4. Descriptions" },
            { id: 5, name: "5. Equipements" },
          ]}
          activeStep={currentStep}
          handleChangeActiveStep={this.handleChange}
        />
        <StepCharacter
          image={image}
          name={name}
          setName={this.handleChange}
          race={race}
          classe={classe}
          characteristics={characteristics}
        />
        {races.length > 0 && (
          <Race
            currentStep={currentStep}
            setRace={this.handleChange}
            race={race}
            races={races}
          />
        )}
        {classes.length > 0 && (
          <Classe
            currentStep={currentStep}
            setClasse={this.handleChange}
            classe={classe}
            classes={classes}
          />
        )}
        <Caracteristiques
          currentStep={currentStep}
          characteristics={characteristics}
          setCharacteristic={this.handleChange}
        />
        <Description currentStep={currentStep} />
        <Equipements currentStep={currentStep} />
        <Button
          onClick={this.createPersonnage}
        >
          Créer le personnage
        </Button>
      </Container>
    );
  }
}

const mapState = state => {
  const { classes } = state.classes
  const { races } = state.races
  return { classes, races };
};

const mapDispatchToProps = {
  getClasses: getClasses,
  getRaces: getRaces
};

export default connect(mapState, mapDispatchToProps)(CreateCharac);
