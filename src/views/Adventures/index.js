import React from "react";
import { Col, Alert, Container } from "react-bootstrap";
import { useMediaQuery } from "react-responsive";
import { Row } from "react-bootstrap";
import { connect } from "react-redux";

import { getAdventures } from "../../redux/actions/adventure.actions";

import NavbarCustom from "../../components/NavbarCustom";
import AdventureCard from "../../components/AdventureCard";
import AdventureTitle from "../../components/AdventureTitle";
import PlayersCard from "../../components/PlayersCard";
import AdventurePersonnalize from "../../components/AdventurePersonnalize";

class Adventures extends React.Component {
  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentWillMount(){
    this.props.getAdventures()
  }

  render(){
    const { adventures, match } = this.props
    const { user } = this.props.user
    const currentAdventure = adventures.find((a) => a.id === Number(match.params.id)) || null
    const isLargeScreen = useMediaQuery({ query: "(min-device-width: 1255px)" });
    return (
      <Container>
        <NavbarCustom
          currentAdventure={currentAdventure ? currentAdventure.name : ''}
        />
        {currentAdventure ? (
          isLargeScreen ? (
            <Row>
              <Col>
                <AdventureCard
                  adventure={currentAdventure}
                  isAdmin={currentAdventure.owner === user.uid}
                />
              </Col>
              <Col>
                <AdventureTitle
                  adventure={currentAdventure}
                  isAdmin={currentAdventure.owner === user.uid}
                />
              </Col>
              <Col>
                <PlayersCard />
              </Col>
              <Col>
                <AdventurePersonnalize />
              </Col>
            </Row>
          ) : (
            <>
              <Row>
                <Col>
                  <AdventureCard
                    adventure={currentAdventure}
                    isAdmin={currentAdventure.owner === user.uid}
                  />
                </Col>
                <Col>
                  <AdventureTitle
                    adventure={currentAdventure}
                    isAdmin={currentAdventure.owner === user.uid}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <PlayersCard />
                </Col>
                <Col>
                  <AdventurePersonnalize />
                </Col>
              </Row>
            </>
          )
        ) : (
          <Alert variant={'danger'}>L'aventure que vous recherchez n'existe pas.</Alert>
        )}
      </Container>
    )
  }
}

const mapState = state => {
  const { user } = state.authentication;
  const { adventures } = state.adventures;
  return { user, adventures };
}

const mapDispatchToProps = {
  getAdventures: getAdventures,
}

export default connect(mapState, mapDispatchToProps)(Adventures);
