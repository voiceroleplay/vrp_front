import React from "react"
import { Route, Redirect } from "react-router-dom"

const RouteWrapper = ({ component: Component, isPrivate, ...rest }) => {
    const signed = localStorage.getItem('user')
    
    if (isPrivate && !signed) {
        return <Redirect to="/login" />;
    }

    if (!isPrivate && signed) {
        return <Redirect to="/" />;
    }

    return (
        <Route
            {...rest}
            component={Component}
        />
    )
}

export default RouteWrapper