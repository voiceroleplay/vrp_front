import React from "react";
import { Switch, Redirect } from "react-router-dom";
import Route from "./Route";

import Login from "../views/Login";
import Register from "../views/Register";
import Dashboard from "../views/Dashboard";
import Characters from "../views/Characters";
import Adventures from "../views/Adventures";
import CreateCharac from "../views/CreateCharac";
import CreateAdventure from "../views/CreateAdventure";
import Story from "../views/Stories";
import CreateStory from "../views/CreateStory";
import Profile from "../views/Profile"

const Routes = () => {
  return (
    <Switch>
      <Route path="/login" exact component={Login} />
      <Route path="/register" component={Register} />

      <Route path="/" exact component={Dashboard} isPrivate />
      <Route path="/profile" exact component={Profile} isPrivate />
      <Route path="/characters/create" component={CreateCharac} isPrivate />
      <Route path="/characters" component={Characters} isPrivate />
      <Route path="/adventures/create" component={CreateAdventure} isPrivate />
      <Route path="/adventures/:id" component={Adventures} isPrivate />
      <Route path="/story/create" component={CreateStory} isPrivate />
      <Route path="/story" component={Story} isPrivate />

      <Redirect from="*" to="/" />
    </Switch>
  );
};

export default Routes;
