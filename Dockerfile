# Stage 1 - the build process
FROM node:latest AS build

RUN rm -rf /usr/src/app/vrp-front
RUN mkdir -p /usr/src/app/vrp-front
WORKDIR /usr/src/app/vrp-front
ENV PATH /usr/src/app/vrp-front/node_modules/.bin:$PATH

COPY package.json ./
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent

COPY . ./
RUN npm run build

# Stage 2 - the production environment
FROM nginx:1.16.0-alpine
COPY --from=build /usr/src/app/vrp-front/build /usr/share/nginx/html/
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 8080 80
CMD ["nginx", "-g", "daemon off;"]
